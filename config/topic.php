<?php 
return [
	'core' => [
		'user' => [
			'insert' => '4.symper_core.user.insert',
			'update' => '4.1.symper_core.user.update'
		],
		'usergroup' => [
			'insert' => '4.symper_core.usergroup.insert',
			'update' => '4.symper_core.usergroup.update'    			
		],
		'user_in_group' => [
			'insert' => '4.symper_core.user_in_group.insert',
			'update' => '4.symper_core.user_in_group.update',
			'remove' => '4.symper_core.user_in_group.remove'
		],
		'task' => [
			'insert' => '4.symper_core.task.insert'
		]
	],
	'event' => [
		'insert' => '4.symper_event.task.insert',
		'update' => '4.symper_event.task.update'
	],
	'task' => [
		'insert' => '4.symper_task.task.insert',
		'update' => '4.symper_task.task.update',
		'remove' => '4.symper_task.task.remove'
	],
	'task_member' => [
		'insert' => '4.symper_task.task_member.insert',
		'remove' => '4.symper_task.task_member.remove'
	],
	'task_comment' => [
		'insert' => '4.symper_task.task_comment.insert',
	],
	'group' => [
		'insert' => '4.symper_task.usergroup.insert',
		'update' => '4.symper_task.usergroup.update',
		'remove' => '4.symper_task.usergroup.remove'
	],
	'user_group' => [
		'insert' => '4.symper_task.user_in_group.insert',
		'remove' => '4.symper_task.user_in_group.remove'
	]
];
?>