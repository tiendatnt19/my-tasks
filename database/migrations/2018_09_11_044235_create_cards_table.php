<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_title', 255);
            $table->string('card_slug', 255)->unique();
            $table->mediumText('card_des')->nullable();
            $table->integer('note_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->integer('status')->default(0);
            $table->tinyInteger('show_des')->default(0);
            $table->tinyInteger('show_checklist')->default(0);
            $table->datetime('start_date')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->tinyInteger('position')->default(1);
            $table->string('from_source', 255)->default('task');
            $table->integer('source_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
