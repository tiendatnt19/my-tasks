<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('card_id')->unsigned()->nulllabel();
            $table->integer('note_id')->unsigned()->nullable();
            $table->text('content');
            $table->foreign('card_id')
                    ->references('id')
                    ->on('cards')
                    ->onDelete('cascade');
            $table->foreign('note_id')
                    ->references('id')
                    ->on('notes')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
