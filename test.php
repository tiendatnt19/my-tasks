<?php
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
error_reporting(E_ALL);

function subscribeActiveMQ($topic){
    subscribeActiveMQPrefix($topic,"symper_core");
    subscribeActiveMQPrefix($topic,"symper_chat");
    subscribeActiveMQPrefix($topic,"symper_task");
}
function subscribeActiveMQPrefix($topic,$prefix){
    $stomp = new Stomp("tcp://localhost:61613","","",array("client-id"=> "$prefix.$topic"));
    $stomp->subscribe("/topic/".$topic,array("activemq.subscriptionName"=> "$prefix.$topic"));
    unset($stomp);
}


