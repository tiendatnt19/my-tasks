# Hướng dẫn cho tester

### **Cài đặt local server và composer để chạy ứng dụng**
Có thể bỏ qua bước này nếu đã cài đặt
* Cài đặt local server (có thể cài xampp, vertrigo). Click vào đường dẫn tương ứng để tải xuống và cài đặt
    * [Xampp](https://www.apachefriends.org/index.html)
    * [Vertrigo](https://www.vswamp.com/)
    * Lưu ý nên tải bản php >= 7.0.0
* Sau khi cài local server xong thì cài composer [tại đây](https://getcomposer.org/download/)

### **Sau đó, mở xampp hoặc Vertrigo server lên, đăng nhập vào phpmyadmin và tạo 1 database tên symper**

### Clone source code về ở bất kỳ thư mục nào. Tạo file .env và dán đoạn sau vào và lưu lại
_Lưu ý thay db username và db password. Với xampp thì tài khoảng mặc định là username=root, password để trống. Với Vertrigo thì mặc định là username=root, password=vertrigo_
>APP_NAME=Laravel
>APP_ENV=local
>APP_KEY=base64:maJ3eYkTDKKD+kaEEJUL7Px7lqn7KeMAE+w6K2PBrWQ=
>APP_DEBUG=true
>APP_URL=http://localhost
>
>LOG_CHANNEL=stack
>
>DB_CONNECTION=mysql
>DB_HOST=127.0.0.1
>DB_PORT=3306
>DB_DATABASE=symper
>DB_USERNAME=root
>DB_PASSWORD=
>
>BROADCAST_DRIVER=log
>CACHE_DRIVER=file
>QUEUE_CONNECTION=sync
>SESSION_DRIVER=file
>SESSION_LIFETIME=120
>
>REDIS_HOST=127.0.0.1
>REDIS_PASSWORD=null
>REDIS_PORT=6379
>
>MAIL_DRIVER=smtp
>MAIL_HOST=smtp.mailtrap.io
>MAIL_PORT=2525
>MAIL_USERNAME=null
>MAIL_PASSWORD=null
>MAIL_ENCRYPTION=null
>
>PUSHER_APP_ID=
>PUSHER_APP_KEY=
>PUSHER_APP_SECRET=
>PUSHER_APP_CLUSTER=mt1
>
>MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
>MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"


### **Mở git bash ở trong thư mục code lên và chạy các lệnh sau**
_(Nếu chưa cài git bash thì tải xuống file cài đặt [tại đây](https://git-scm.com/downloads))_

1. composer dump-autoload
2. php artisan migrate:refresh
3. php artisan serve

### Sau khi chạy lệnh số 3 sẽ được kết quả như hình
![Kết quả](https://i.imgur.com/rLUEGIn.png)

Truy cập vào site thông qua đường dẫn này

# DEV

### Cài đặt node module
>*npm install*
### Lệnh compile code
>*npm run dev*