-- phpMiniAdmin dump 1.9.170730
-- Datetime: 2018-10-16 22:11:26
-- Host: 127.0.0.1
-- Database: task_symper_vn

/*!40030 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

DROP TABLE IF EXISTS `card_files`;
CREATE TABLE `card_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` int(10) unsigned NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `card_files` DISABLE KEYS */;
INSERT INTO `card_files` VALUES ('24','9','24','2018-10-05 19:18:28','2018-10-05 19:18:28'),('25','9','25','2018-10-05 19:27:10','2018-10-05 19:27:10'),('26','9','26','2018-10-05 19:28:02','2018-10-05 19:28:02'),('27','19','27','2018-10-07 05:02:24','2018-10-07 05:02:24'),('29','21','29','2018-10-08 04:03:33','2018-10-08 04:03:33'),('30','23','30','2018-10-08 05:02:45','2018-10-08 05:02:45'),('31','64','31','2018-10-10 10:09:45','2018-10-10 10:09:45'),('33','29','33','2018-10-16 15:42:16','2018-10-16 15:42:16');
/*!40000 ALTER TABLE `card_files` ENABLE KEYS */;

DROP TABLE IF EXISTS `card_labels`;
CREATE TABLE `card_labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` int(10) unsigned NOT NULL,
  `label_name` varchar(255) DEFAULT NULL,
  `check` tinyint(4) NOT NULL DEFAULT '0',
  `label_pos` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `card_labels` DISABLE KEYS */;
/*!40000 ALTER TABLE `card_labels` ENABLE KEYS */;

DROP TABLE IF EXISTS `card_members`;
CREATE TABLE `card_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` int(10) unsigned NOT NULL,
  `member_id` int(11) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 là user, 2 là group',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `card_members` DISABLE KEYS */;
INSERT INTO `card_members` VALUES ('1','6','9','1','2018-10-04 07:33:10','2018-10-04 07:33:10'),('2','7','9','1','2018-10-04 07:40:00','2018-10-04 07:40:00'),('3','8','9','1','2018-10-04 07:41:26','2018-10-04 07:41:26'),('4','9','9','1','2018-10-04 09:08:28','2018-10-04 09:08:28'),('5','10','9','1','2018-10-04 09:49:18','2018-10-04 09:49:18'),('6','19','9','1','2018-10-06 04:23:55','2018-10-06 04:23:55'),('7','9','60','1','2018-10-06 08:28:09','2018-10-06 08:28:09'),('8','9','7','1','2018-10-06 08:30:22','2018-10-06 08:30:22'),('9','20','9','1','2018-10-06 15:09:45','2018-10-06 15:09:45'),('10','21','9','1','2018-10-06 15:09:50','2018-10-06 15:09:50'),('11','22','9','1','2018-10-06 15:09:58','2018-10-06 15:09:58'),('12','23','9','1','2018-10-06 16:02:50','2018-10-06 16:02:50'),('13','24','9','1','2018-10-07 15:48:10','2018-10-07 15:48:10'),('14','25','9','1','2018-10-07 15:48:36','2018-10-07 15:48:36'),('15','26','9','1','2018-10-07 15:48:39','2018-10-07 15:48:39'),('16','27','9','1','2018-10-07 15:48:43','2018-10-07 15:48:43'),('17','28','9','1','2018-10-07 15:49:49','2018-10-07 15:49:49'),('18','29','9','1','2018-10-07 15:51:21','2018-10-07 15:51:21'),('19','30','9','1','2018-10-07 15:56:28','2018-10-07 15:56:28'),('20','31','9','1','2018-10-07 15:57:05','2018-10-07 15:57:05'),('21','32','9','1','2018-10-08 00:57:34','2018-10-08 00:57:34'),('22','33','9','1','2018-10-08 01:05:56','2018-10-08 01:05:56'),('23','37','9','1','2018-10-08 04:50:30','2018-10-08 04:50:30'),('24','39','9','1','2018-10-08 05:05:30','2018-10-08 05:05:30'),('25','41','9','1','2018-10-08 05:10:22','2018-10-08 05:10:22'),('26','42','9','1','2018-10-08 05:11:28','2018-10-08 05:11:28'),('27','43','9','1','2018-10-08 05:12:44','2018-10-08 05:12:44'),('28','44','9','1','2018-10-08 05:27:49','2018-10-08 05:27:49'),('29','45','9','1','2018-10-08 07:06:48','2018-10-08 07:06:48'),('30','46','9','1','2018-10-08 07:18:35','2018-10-08 07:18:35'),('31','47','9','1','2018-10-08 07:23:02','2018-10-08 07:23:02'),('32','48','9','1','2018-10-08 07:25:19','2018-10-08 07:25:19'),('33','49','9','1','2018-10-08 07:34:24','2018-10-08 07:34:24'),('34','50','9','1','2018-10-08 07:34:57','2018-10-08 07:34:57'),('35','51','9','1','2018-10-08 07:35:45','2018-10-08 07:35:45'),('36','52','9','1','2018-10-08 07:39:48','2018-10-08 07:39:48'),('37','53','9','1','2018-10-08 07:39:59','2018-10-08 07:39:59'),('38','56','9','1','2018-10-08 11:24:51','2018-10-08 11:24:51'),('39','61','9','1','2018-10-10 01:32:13','2018-10-10 01:32:13'),('40','62','9','1','2018-10-10 01:36:27','2018-10-10 01:36:27'),('41','64','60','1',NULL,NULL),('42','64','9','1','2018-10-10 01:39:08','2018-10-10 01:39:08'),('43','56','60','1','2018-10-12 01:27:07','2018-10-12 01:27:07'),('44','39','60','1','2018-10-12 13:44:25','2018-10-12 13:44:25'),('45','64','7','1','2018-10-13 10:25:31','2018-10-13 10:25:31'),('46','64','27','1','2018-10-13 10:26:19','2018-10-13 10:26:19'),('47','9','27','1','2018-10-15 05:01:12','2018-10-15 05:01:12'),('48','84','9','1','2018-10-15 08:40:14','2018-10-15 08:40:14'),('49','85','9','1','2018-10-15 08:40:38','2018-10-15 08:40:38'),('50','86','9','1',NULL,NULL),('51','87','60','1',NULL,NULL),('52','87','9','1',NULL,NULL),('53','87','7','1',NULL,NULL),('54','87','27','1',NULL,NULL),('55','88','9','1',NULL,NULL),('56','89','9','1','2018-10-16 04:31:43','2018-10-16 04:31:43'),('57','90','9','1','2018-10-16 14:58:20','2018-10-16 14:58:20'),('58','91','9','1','2018-10-16 15:02:26','2018-10-16 15:02:26'),('59','91','60','1','2018-10-16 15:03:02','2018-10-16 15:03:02'),('60','92','9','1','2018-10-16 15:03:31','2018-10-16 15:03:31'),('61','92','60','1','2018-10-16 15:04:02','2018-10-16 15:04:02'),('62','93','9','1','2018-10-16 15:06:51','2018-10-16 15:06:51'),('63','94','9','1','2018-10-16 15:07:03','2018-10-16 15:07:03'),('64','95','9','1','2018-10-16 15:08:55','2018-10-16 15:08:55'),('65','96','9','1','2018-10-16 15:09:01','2018-10-16 15:09:01'),('66','97','9','1','2018-10-16 15:09:23','2018-10-16 15:09:23'),('67','98','9','1','2018-10-16 15:17:30','2018-10-16 15:17:30'),('68','99','9','1','2018-10-16 15:17:31','2018-10-16 15:17:31'),('69','100','9','1','2018-10-16 15:17:57','2018-10-16 15:17:57'),('70','101','9','1','2018-10-16 15:33:46','2018-10-16 15:33:46'),('71','102','9','1',NULL,NULL),('72','103','9','1',NULL,NULL),('73','104','9','1',NULL,NULL),('74','105','9','1',NULL,NULL),('75','104','60','1','2018-10-16 16:20:12','2018-10-16 16:20:12'),('76','105','60','1','2018-10-16 16:20:28','2018-10-16 16:20:28'),('77','105','27','1','2018-10-16 16:22:14','2018-10-16 16:22:14'),('78','106','9','1',NULL,NULL),('79','107','9','1','2018-10-16 21:25:04','2018-10-16 21:25:04');
/*!40000 ALTER TABLE `card_members` ENABLE KEYS */;

DROP TABLE IF EXISTS `cards`;
CREATE TABLE `cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_title` varchar(255) NOT NULL,
  `card_slug` varchar(255) NOT NULL,
  `card_des` mediumtext,
  `note_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `show_des` tinyint(4) NOT NULL DEFAULT '0',
  `show_checklist` tinyint(4) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `position` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `from_source` varchar(25) DEFAULT 'task',
  `source_id` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) DEFAULT NULL,
  `last_position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES ('3','Thêm mới','them-moi-1','','2','1','2','0','0','2018-10-03 09:30:00','2018-10-03 10:00:00','1','2018-10-04 08:36:15','2018-10-04 08:36:15','event','665',NULL,NULL),('9','Chào Đạt','chao-Đat','MÌnh cũng là Đạt','6','9','1','1','1','2018-10-28 00:00:00','2018-10-11 00:00:00','8','2018-10-04 09:08:28','2018-10-16 04:43:06','task','9',NULL,NULL),('10','Số 1','so-1-1538973965',NULL,'8','9','2','0','0','2018-10-04 09:49:18','2018-10-04 09:49:18','2','2018-10-04 09:49:18','2018-10-16 15:08:21','task','10',NULL,NULL),('11','Lấy lại tài khoản webmail','lay-lai-tai-khoan-webmail','Testing','3','23','0','0','0','2018-10-04 08:45:59','2019-05-01 00:00:00','1','2018-10-04 08:45:59','2018-10-04 08:45:59','core','122','{\"Manual\":\"\"}',NULL),('13','Test Thêm mới Task','test-them-moi-task','Thêm mới Task','3','7','0','0','0','2018-10-04 11:55:31','2018-04-10 00:00:00','1','2018-10-04 11:55:31','2018-10-04 11:55:31','core','123','{\"Manual\":\"\"}',NULL),('14','Gán công việc','gan-cong-viec','Công việc được giao','3','7','0','0','0','2018-10-04 11:56:08','2018-04-10 00:00:00','1','2018-10-04 11:56:08','2018-10-04 11:56:08','core','124','{\"Manual\":\"\"}',NULL),('15','có File đấy 13','co-file-day-13','','2','1','1','0','0','2018-10-01 09:30:00','2018-10-01 10:00:00','1','2018-10-01 09:34:28','2018-10-04 17:27:00','event','655',NULL,NULL),('16','Thêm mới','them-moi','','2','1','1','0','0','2018-10-02 09:30:00','2018-10-02 10:00:00','1','2018-10-04 08:33:31','2018-10-04 17:28:14','event','663',NULL,NULL),('17','Thêm mới1','them-moi1','','2','1','1','0','0','2018-10-02 08:30:00','2018-10-02 09:00:00','1','2018-10-04 08:33:03','2018-10-05 09:56:06','event','662',NULL,NULL),('18','Test Lặp','test-lap','','2','1','1','0','0','2018-10-02 17:54:11','2018-10-02 22:54:11','1','2018-09-27 17:55:01','2018-10-05 11:43:04','event','653',NULL,NULL),('19','Số 3','so-3','dsadasdsa','4','9','0','0','0','2018-10-06 04:23:55','2018-10-06 04:23:55','7','2018-10-06 04:23:55','2018-10-12 09:24:34','task','19',NULL,NULL),('20','Số 1-2','so-1-2-1538974212','Thẻ số 2 của bảng task','5','9','1','1','0','2018-10-06 15:09:45','2018-10-06 15:09:45','6','2018-10-06 15:09:45','2018-10-16 02:37:12','task','20',NULL,'2'),('21','Số 2-5','so-2-5',NULL,'9','9','1','0','0','2018-10-06 15:09:50','2018-10-06 15:09:50','4','2018-10-06 15:09:50','2018-10-12 13:52:13','task','21',NULL,NULL),('22','Số 2-2','so-2-2',NULL,'6','9','0','0','0','2018-10-06 15:09:58','2018-10-10 00:00:00','5','2018-10-06 15:09:58','2018-10-16 04:43:06','task','22',NULL,NULL),('23','Gửi proposal','gui-proposal','Làm proposal gồm cả profile của nhân viên','5','9','1','1','1','2018-10-06 16:02:50','2018-10-07 00:00:00','2','2018-10-06 16:02:50','2018-10-16 02:37:12','task','23',NULL,'4'),('24','Số 1-2','so-1-2',NULL,'6','9','2','0','0','2018-10-07 15:48:10','2018-10-07 15:48:10','3','2018-10-07 15:48:10','2018-10-16 04:43:06','task','24',NULL,NULL),('25','Số 2-1','so-2-1',NULL,'9','9','2','0','0','2018-10-07 15:48:36','2018-10-07 15:48:36','3','2018-10-07 15:48:36','2018-10-12 13:52:13','task','25',NULL,NULL),('26','Số 2-3','so-2-3','ádadsjkdjsjsdhfsdjfshdjkfhsdjfhskdhfksjdhfjkshdfkjsdhkjfshdkjhfkshfksdhfksdhkdshkjfsdhjkdshfsdf','8','9','2','1','1','2018-10-07 15:48:39','2018-10-26 00:00:00','6','2018-10-07 15:48:39','2018-10-16 15:08:21','task','26',NULL,NULL),('27','Số 2-4','so-2-4',NULL,'5','9','2','0','1','2018-10-07 15:48:43','2018-10-08 00:00:00','2','2018-10-07 15:48:43','2018-10-15 08:50:53','task','27',NULL,'8'),('28','Số 2-6','so-2-6',NULL,'7','9','2','0','0','2018-10-07 15:49:49','2018-10-07 15:49:49','2','2018-10-07 15:49:49','2018-10-16 04:42:43','task','28',NULL,'4'),('29','3g34g4g4','3g34g4g4','http://task.symper.vn/3ABRJ5VC/3g34g4g4','7','9','1','0','0','2018-10-07 15:51:21','2018-10-16 00:00:00','3','2018-10-07 15:51:21','2018-10-16 15:45:58','task','29',NULL,'8'),('30','34g43g43g43g','34g43g43g43g',NULL,'4','9','1','0','0','2018-10-07 15:56:28','2018-10-07 15:56:28','6','2018-10-07 15:56:28','2018-10-12 03:24:39','task','30',NULL,NULL),('31','nguyen','nguyen',NULL,'8','9','1','0','0','2018-10-07 15:57:05','2018-10-07 15:57:05','4','2018-10-07 15:57:05','2018-10-16 15:08:21','task','31',NULL,NULL),('32','Số 2','so-2-1538973975',NULL,'9','9','1','0','0','2018-10-08 00:57:34','2018-10-08 00:57:34','7','2018-10-08 00:57:34','2018-10-13 13:05:28','task','32',NULL,NULL),('33','Test giao viec','test-giao-viec',NULL,'8','9','0','1','1','2018-10-08 01:05:56','2018-10-10 00:00:00','3','2018-10-08 01:05:56','2018-10-16 15:08:21','task','33',NULL,NULL),('34','Event 2','event-2','','2','9','1','0','0','2018-10-08 08:43:00','2018-10-08 09:13:00','1','2018-10-08 08:45:11','2018-10-08 10:30:08','event','697',NULL,NULL),('35','Sửa','sua','ádsa','2','1','1','0','0','2018-10-04 12:45:00','2019-05-01 04:00:00','1','0000-00-00 00:00:00','2018-10-08 11:17:01','event','666',NULL,NULL),('36','Lấy lại tài khoản webmail','lay-lai-tai-khoan-webmail-1','Testing','2','23','2','0','0','2018-10-04 08:45:00','2019-05-01 00:00:00','1','0000-00-00 00:00:00','2018-10-08 11:17:57','event','672',NULL,NULL),('37','Số 1-0','so-1-0','Thẻ số 1 của bảng task','5','9','1','1','1','2018-10-08 04:50:30','2018-10-08 04:50:30','7','2018-10-08 04:50:30','2018-10-16 02:37:12','task','37',NULL,'2'),('38','Lặp hàng ngày 27/9 - 27/10','lap-hang-ngay-27-9---27-10','','2','1','2','0','0','2018-10-11 17:28:00','2018-10-11 19:28:00','1','2018-09-27 17:30:01','2018-10-08 12:02:03','event','616',NULL,NULL),('39','3f23f3f3f3f3\'','3f23f3f3f3f3',NULL,'9','9','1','1','0','2018-10-08 05:05:30','2018-10-08 05:05:30','5','2018-10-08 05:05:30','2018-10-12 13:52:13','task','39',NULL,NULL),('40','Test gửi','test-gui','ákdjfasdjf','3','7','0','0','0','2018-10-08 12:06:21','2018-08-10 00:00:00','1','2018-10-08 12:06:21','2018-10-08 12:06:21','core','125','{\"Manual\":\"\"}',NULL),('41','dinh test','dinh-test',NULL,'4','9','0','0','0','2018-10-08 05:10:22','2018-10-08 05:10:22','5','2018-10-08 05:10:22','2018-10-10 03:03:54','task','41',NULL,NULL),('42','dinh test lần 2','dinh-test-lan-2',NULL,'4','9','0','0','0','2018-10-11 00:00:00','2018-10-31 00:00:00','3','2018-10-08 05:11:28','2018-10-10 03:03:54','task','42',NULL,NULL),('43','ok','ok',NULL,'4','9','0','0','0','2018-10-08 05:12:44','2018-10-08 05:12:44','2','2018-10-08 05:12:44','2018-10-10 03:03:54','task','43',NULL,NULL),('44','dinh test lan 2','dinh-test-lan-2-1538976469',NULL,'6','9','0','0','0','2018-10-08 05:27:49','2018-10-08 05:27:49','6','2018-10-08 05:27:49','2018-10-16 04:43:06','task','44',NULL,NULL),('45','kjkljl','kjkljl',NULL,'5','9','1','1','0','2018-10-08 07:06:48','2018-10-08 00:00:00','3','2018-10-08 07:06:48','2018-10-16 02:37:12','task','45',NULL,'2'),('46','test','test',NULL,'6','9','2','0','0','2018-10-08 07:18:35','2018-10-08 07:18:35','2','2018-10-08 07:18:35','2018-10-16 04:43:06','task','46',NULL,NULL),('47','test 2','test-2','lllkegwgwegewgwege','9','9','2','0','0','2018-10-08 07:23:02','2018-10-08 07:23:02','6','2018-10-08 07:23:02','2018-10-12 14:15:21','task','47',NULL,NULL),('48','test 3123','test-3123','dsadsadadsa\ndsa\ndsa\ndsa\nd\nsa\ndsa\ndas\nd\nas\nd\nsa\nd\nas','5','9','1','1','0','2018-10-08 07:25:19','2018-10-08 07:25:19','5','2018-10-08 07:25:19','2018-10-16 02:37:12','task','48',NULL,'2'),('49','Số 2-7','so-2-7',NULL,'4','9','0','0','0','2018-10-08 07:34:24','2018-10-08 07:34:24','4','2018-10-08 07:34:24','2018-10-10 03:03:54','task','49',NULL,NULL),('50','Số 2-8','so-2-8',NULL,'6','9','1','0','0','2018-10-08 07:34:57','2018-10-08 07:34:57','4','2018-10-08 07:34:57','2018-10-16 04:43:06','task','50',NULL,NULL),('51','Số 2-9','so-2-9',NULL,'9','9','0','0','0','2018-10-08 07:35:45','2018-10-08 07:35:45','2','2018-10-08 07:35:45','2018-10-12 13:52:13','task','51',NULL,NULL),('52','dinhnv','dinhnv',NULL,'5','9','1','0','0','2018-10-08 07:39:48','2018-10-08 07:39:48','4','2018-10-08 07:39:48','2018-10-16 02:37:12','task','52',NULL,'2'),('53','test aokewegewge123','test-aokewegewge123',NULL,'5','9','1','0','0','2018-10-08 07:39:59','2018-10-08 07:39:59','8','2018-10-08 07:39:59','2018-10-16 02:37:12','task','53',NULL,'2'),('54','Event 5','event-5','','2','9','1','0','0','2018-10-07 03:30:00','2018-10-07 04:00:00','1','2018-10-08 14:58:00','2018-10-08 14:58:12','event','734',NULL,NULL),('55','Test giao viec','test-giao-viec-1','','2','9','2','0','0','2018-10-08 03:35:56','2018-10-11 02:30:00','1','0000-00-00 00:00:00','2018-10-08 17:28:15','event','716',NULL,NULL),('56','Đạt test 1','Đat-test-1',NULL,'8','9','1','0','0','2018-10-08 11:24:51','2018-10-31 00:00:00','5','2018-10-08 11:24:51','2018-10-16 15:08:21','task','56',NULL,'4'),('57','Số 2-2','so-2-2-1','','2','9','2','0','0','2018-10-06 15:09:00','2018-10-10 00:00:00','1','0000-00-00 00:00:00','2018-10-09 15:19:45','event','705',NULL,NULL),('58','Event 6','event-6','','12','21','1','0','0','2018-10-08 16:24:00','2018-10-08 16:54:00','4','2018-10-08 16:24:40','2018-10-16 15:18:05','event','736',NULL,NULL),('59','Event 1','event-1','','2','9','1','0','0','2018-10-08 00:00:00','2018-10-08 00:00:00','1','2018-10-08 08:23:18','2018-10-10 08:08:26','event','691',NULL,NULL),('64','thanh vien','thanh-vien-1539135548',NULL,'5','9','0','1','1','2018-10-10 01:39:08','2018-10-10 01:39:08','11','2018-10-10 01:39:08','2018-10-16 02:37:12','task','64',NULL,NULL),('65','Tết','tet','','12','9','1','0','0','2018-10-07 09:30:00','2018-10-07 11:30:00','1','2018-10-10 09:45:21','2018-10-10 09:46:14','event','743',NULL,NULL),('66','6-8','6-8','','2','9','1','0','0','2018-10-10 06:00:00','2018-10-10 12:00:00','1','2018-10-10 10:16:58','2018-10-10 10:25:10','event','748',NULL,NULL),('67','11/10 123','11-10-123','','3','9','1','0','0','2018-10-11 00:00:00','2018-10-12 00:00:00','1','2018-10-10 10:45:48','2018-10-10 10:48:09','event','753',NULL,NULL),('68','Thuộc group 1','thuoc-group-1','','2','7','1','0','0','2018-10-10 00:00:00','2018-10-10 12:00:00','1','2018-10-10 11:06:54','2018-10-10 11:07:30','event','756',NULL,NULL),('82','34g43g43g43g','34g43g43g43g-1',NULL,'5','9','1','0','0','2018-10-07 15:56:28','2018-10-07 15:56:28','9','2018-10-07 15:56:28','2018-10-16 02:37:12','task','30',NULL,'0'),('84','test thêm mới 321','test-them-moi-321',NULL,'4','9','0','0','0','2018-10-15 08:40:14','2018-10-15 00:00:00','9','2018-10-15 08:40:14','2018-10-15 08:46:15','task','84',NULL,NULL),('85','test thêm mới 123','test-them-moi-123',NULL,'5','9','0','0','0','2018-10-15 08:40:38','2018-10-15 00:00:00','12','2018-10-15 08:40:38','2018-10-16 02:37:12','task','85',NULL,NULL),('87','thanh vien','thanh-vien',NULL,'5','9','0','1','1','2018-10-10 01:39:08','2018-10-10 01:39:08','10','2018-10-10 01:39:08','2018-10-16 02:37:12','task','64',NULL,'0'),('88','Số 2-6','so-2-6-1',NULL,'6','9','1','1','0','2018-10-07 15:49:49','2018-10-16 00:00:00','7','2018-10-07 15:49:49','2018-10-16 15:12:11','task','28',NULL,'0'),('89','test','test-1',NULL,'13','9','0','0','0','2018-10-16 04:31:43','2018-10-16 04:31:43','1','2018-10-16 04:31:43','2018-10-16 04:31:43','task','89',NULL,NULL),('90','fsdf','fsdf','fdgdgdfdgdfg','6','9','1','1','1','2018-10-16 14:58:20','2018-10-17 00:00:00','2','2018-10-16 14:58:20','2018-10-16 14:58:41','task','90',NULL,'2'),('91','TEST01','test01','TEST 01','14','9','1','1','1','2018-10-16 00:00:00','2018-10-16 00:00:00','2','2018-10-16 15:02:26','2018-10-16 16:19:06','task','91',NULL,'2'),('92','TEST02','test02',NULL,'15','9','1','0','0','2018-10-17 00:00:00','2018-10-17 00:00:00','1','2018-10-16 15:03:31','2018-10-16 15:03:44','task','92',NULL,NULL),('93','Tan','tan',NULL,'16','9','2','0','0','2018-10-16 15:06:51','2018-10-16 15:06:51','2','2018-10-16 15:06:51','2018-10-16 15:07:51','task','93',NULL,'1'),('94','Tantna','tantna',NULL,'17','9','2','0','0','2018-10-16 15:07:03','2018-10-16 15:07:03','2','2018-10-16 15:07:03','2018-10-16 15:07:53','task','94',NULL,'1'),('95','TEST01','test01-1',NULL,'19','9','0','0','0','2018-10-16 15:08:55','2018-10-16 15:08:55','2','2018-10-16 15:08:55','2018-10-16 15:09:07','task','95',NULL,NULL),('96','TETST02','tetst02',NULL,'18','9','0','0','0','2018-10-16 15:09:01','2018-10-16 15:09:01','2','2018-10-16 15:09:01','2018-10-16 15:09:16','task','96',NULL,NULL),('97','TEST03','test03',NULL,'19','9','0','0','0','2018-10-16 15:09:23','2018-10-16 15:09:23','3','2018-10-16 15:09:23','2018-10-16 15:09:23','task','97',NULL,NULL),('98','Tan02','tan02',NULL,'20','9','0','0','0','2018-10-16 15:17:30','2018-10-16 15:17:30','2','2018-10-16 15:17:30','2018-10-16 15:33:56','task','98',NULL,NULL),('99','TAn01','tan01',NULL,'21','9','0','0','0','2018-10-16 15:17:31','2018-10-16 15:17:31','2','2018-10-16 15:17:31','2018-10-16 15:17:42','task','99',NULL,NULL),('100','TAntan002','tantan002',NULL,'12','9','0','0','0','2018-10-16 15:17:57','2018-10-16 15:17:57','2','2018-10-16 15:17:57','2018-10-16 15:18:05','task','100',NULL,NULL),('101','TAntan','tantan',NULL,'21','9','0','0','0','2018-10-16 15:33:46','2018-10-16 15:33:46','3','2018-10-16 15:33:46','2018-10-16 15:33:56','task','101',NULL,NULL),('102','3g34g4g4','3g34g4g4-1','http://task.symper.vn/3ABRJ5VC/3g34g4g4','11','9','1','0','0','2018-10-07 15:51:21','2018-10-16 00:00:00','12','2018-10-07 15:51:21','2018-10-16 15:45:58','task','29',NULL,'0'),('103','Số 2-5','so-2-5-1',NULL,'13','9','1','0','0','2018-10-06 15:09:50','2018-10-06 15:09:50','12','2018-10-06 15:09:50','2018-10-12 13:52:13','task','21',NULL,'0'),('104','3g34g4g4','3g34g4g4-2','http://task.symper.vn/3ABRJ5VC/3g34g4g4','13','9','1','0','0','2018-10-07 15:51:21','2018-10-16 00:00:00','12','2018-10-07 15:51:21','2018-10-16 15:45:58','task','29',NULL,'0'),('105','3g34g4g4','3g34g4g4-3','http://task.symper.vn/3ABRJ5VC/3g34g4g4','13','9','0','0','0','2018-10-07 15:51:21','2018-10-16 00:00:00','12','2018-10-07 15:51:21','2018-10-16 16:16:27','task','29',NULL,'0'),('106','Số 3','so-3-1','dsadasdsa','5','9','0','0','0','2018-10-06 04:23:55','2018-10-06 04:23:55','12','2018-10-06 04:23:55','2018-10-12 09:24:34','task','19',NULL,'0'),('107','đạt test','dat-test',NULL,'22','9','0','0','0','2018-10-16 21:25:04','2018-10-16 21:25:04','1','2018-10-16 21:25:04','2018-10-16 21:25:04','task','107',NULL,NULL);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;

DROP TABLE IF EXISTS `checklists`;
CREATE TABLE `checklists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` int(10) unsigned NOT NULL,
  `content` mediumtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `checklists` DISABLE KEYS */;
INSERT INTO `checklists` VALUES ('1','47','wegewg','0','2018-10-08 07:26:23','2018-10-08 07:26:23'),('2','27','sadasdasd','1','2018-10-08 11:58:57','2018-10-14 12:00:48'),('3','27','adasdasd','1','2018-10-08 11:58:58','2018-10-14 12:00:49'),('4','27','ádasd','1','2018-10-08 11:58:59','2018-10-14 12:00:50'),('5','27','âdasd','1','2018-10-08 12:27:44','2018-10-15 07:36:01'),('6','27','ádasd','1','2018-10-08 12:27:45','2018-10-15 07:36:11'),('7','27','ádasd','1','2018-10-08 12:27:46','2018-10-15 07:36:04'),('8','27','ádasd','0','2018-10-08 12:27:46','2018-10-14 11:56:11'),('9','27','ádasd','0','2018-10-08 12:27:47','2018-10-14 11:56:12'),('10','26','adsdasdasd','1','2018-10-10 02:12:35','2018-10-13 13:55:08'),('11','26','ádasda','1','2018-10-10 02:12:35','2018-10-13 13:55:06'),('12','26','ádasd','1','2018-10-10 02:12:36','2018-10-13 16:15:18'),('13','26','ádasd','1','2018-10-10 02:12:37','2018-10-13 02:22:32'),('14','26','ádasd','1','2018-10-10 02:12:37','2018-10-13 02:22:33'),('15','26','ádasd','1','2018-10-10 02:12:38','2018-10-13 02:25:27'),('16','26','sadas','1','2018-10-10 02:12:39','2018-10-13 02:25:28'),('17','64','fasd','0','2018-10-10 10:20:56','2018-10-15 07:45:43'),('18','64','fasdfsfdsa','0','2018-10-10 10:20:57','2018-10-15 07:45:42'),('19','91','A','1','2018-10-16 15:02:48','2018-10-16 15:50:46'),('20','91','B','1','2018-10-16 15:02:49','2018-10-16 15:50:48'),('21','91','C','1','2018-10-16 15:02:49','2018-10-16 15:50:48'),('22','91','D','1','2018-10-16 15:02:50','2018-10-16 15:50:49'),('23','91','E','1','2018-10-16 15:02:51','2018-10-16 15:50:51'),('24','29','dddddddf','0','2018-10-16 15:42:00','2018-10-16 15:42:07');
/*!40000 ALTER TABLE `checklists` ENABLE KEYS */;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` int(10) unsigned NOT NULL,
  `content` mediumtext NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES ('1','23','rgdgdf','9','0','2018-10-06 16:05:06','2018-10-06 16:05:06'),('2','23','gdfsgs','9','0','2018-10-07 13:48:01','2018-10-07 13:48:01'),('3','23','test thao luan','9','0','2018-10-08 04:02:55','2018-10-08 04:02:55'),('4','23','best thảo luận','7','0','2018-10-08 04:03:13','2018-10-08 04:03:13'),('5','23','test','9','0','2018-10-08 04:59:53','2018-10-08 04:59:53'),('6','47','wegwge','9','0','2018-10-08 07:26:18','2018-10-08 07:26:18'),('7','47','egnwegnwege','9','0','2018-10-08 07:27:10','2018-10-08 07:27:10'),('8','23','fdjsfds','9','0','2018-10-09 01:43:09','2018-10-09 01:43:09'),('9','23','fbhdsfsad','9','0','2018-10-09 01:43:10','2018-10-09 01:43:10'),('10','33','ádasd','9','0','2018-10-09 01:43:32','2018-10-09 01:43:32'),('11','33','ádasd','9','0','2018-10-09 01:43:33','2018-10-09 01:43:33'),('12','33','ádasd','9','0','2018-10-09 01:43:33','2018-10-09 01:43:33'),('13','33','ád','9','0','2018-10-09 01:43:34','2018-10-09 01:43:34'),('14','33','ád','9','0','2018-10-09 01:43:35','2018-10-09 01:43:35'),('15','39','ádad','9','0','2018-10-09 01:43:44','2018-10-09 01:43:44'),('16','39','ádasd','9','0','2018-10-09 01:43:45','2018-10-09 01:43:45'),('17','42','ádsd','9','0','2018-10-09 07:25:21','2018-10-09 07:25:21'),('18','42','ads','9','0','2018-10-09 07:25:22','2018-10-09 07:25:22'),('19','42','ádasd','9','0','2018-10-09 07:25:23','2018-10-09 07:25:23'),('20','64','sfsafd','7','0','2018-10-10 10:20:42','2018-10-10 10:20:42'),('21','64','fasfdsa','9','0','2018-10-10 10:20:44','2018-10-10 10:20:44'),('22','64','sfsfdsa','9','0','2018-10-10 10:20:49','2018-10-10 10:20:49'),('23','9','lkjlj','9','0','2018-10-11 14:52:01','2018-10-11 14:52:01'),('24','88','klkjljl','9','0','2018-10-16 16:02:09','2018-10-16 16:02:09'),('25','88','fasdfasdf','9','0','2018-10-16 16:06:49','2018-10-16 16:06:49'),('26','105','dvsf','9','0','2018-10-16 16:20:33','2018-10-16 16:20:33');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(50) NOT NULL,
  `file_url` varchar(255) NOT NULL,
  `author` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_size` int(11) NOT NULL,
  `file_content_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES ('24','2u---david-guetta_-justin-bieber-[320kbps_mp3]','mp3','attachements/3eT1p0Qxk279ZbNcsClIvForFaCmAugWZfUFQYib.mpga','9','2018-10-05 19:18:28','2018-10-05 19:18:28','7812825','audio/mpeg'),('25','user_2','jpg','attachements/JlIDCwlTc6BxYYXd2ebhxAH2L4w1dV1dDLGzkxW9.jpeg','9','2018-10-05 19:27:10','2018-10-05 19:27:10','100808','image/jpeg'),('26','img','jfif','attachements/DCYlrh5cESRCkIpV9d6boshlBu3SOnfM8WNBLdDx.jpeg','9','2018-10-05 19:28:02','2018-10-05 19:28:02','32798','image/jpeg'),('27','user_2','jpg','attachements/qS009WSmOxpLlm73ugJNqAgIftRpWwIvVivGd9Bx.jpeg','9','2018-10-07 05:02:24','2018-10-07 05:02:24','100808','image/jpeg'),('29','user_2','jpg','attachements/tWvm5MT67S3qeOTkwyLUk0VxRU1rRPRCJveys0bJ.jpeg','9','2018-10-08 04:03:33','2018-10-08 04:03:33','100808','image/jpeg'),('30','login_form','xd','attachements/W24pFod1ZmmgxyeGWbAVCMRWD79UAZPTE3XEYfYp.zip','9','2018-10-08 05:02:45','2018-10-08 05:02:45','2302685','application/zip'),('31','1','JPG','attachements/8bENxYzJfH1uSodMCMZPIxpCraGwGoHojm5jU4Yt.jpeg','9','2018-10-10 10:09:45','2018-10-10 10:09:45','20531','image/jpeg'),('33','capture','JPG','attachements/tXgmR0bKyRYkFUVqruNj26RJBnkVpTYlKdVHGQXB.jpeg','9','2018-10-16 15:42:16','2018-10-16 15:42:16','91803','image/jpeg');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `card_id` int(10) unsigned NOT NULL,
  `note_id` int(10) unsigned DEFAULT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `type` varchar(25) NOT NULL DEFAULT 'user_create',
  `position` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
INSERT INTO `notes` VALUES ('1','test lai phat','test-lai-phat','1','0','user_create','1','2018-10-02 01:56:45','2018-10-02 01:56:45'),('2','Công việc được đồng bộ','cong-viec-duoc-dong-bo','1','1','sync','1','2018-10-04 04:09:07','2018-10-04 04:09:07'),('3','Công việc được đồng bộ','cong-viec-duoc-dong-bo-1','7','0','sync','1','2018-10-04 04:48:09','2018-10-04 04:48:09'),('4','test','test','9','0','user_create','3','2018-10-04 07:32:46','2018-10-16 02:37:12'),('5','TASK','task','9','0','user_create','2','2018-10-05 03:32:47','2018-10-16 02:37:12'),('6','weh5h54j5','weh5h54j5','9','1','user_create','12','2018-10-06 15:09:47','2018-10-16 15:08:29'),('7','fdsf3434g','fdsf3434g','9','1','user_create','10','2018-10-06 16:10:39','2018-10-16 15:08:29'),('8','Task','task-1539353752','9','1','user_create','9','2018-10-07 15:51:28','2018-10-16 15:08:29'),('9','Task ngay 8/10','task-ngay-8-10','9','1','user_create','7','2018-10-08 01:05:32','2018-10-16 15:08:29'),('10','hjhjhté','hjhjhte','9','1','user_create','5','2018-10-08 12:30:37','2018-10-16 15:08:29'),('11','tạo bảng mới','tao-bang-moi','9','1','user_create','8','2018-10-09 02:42:13','2018-10-16 15:08:29'),('12','Công việc được đồng bộ','cong-viec-duoc-dong-bo-3','9','2','sync','5','2018-10-09 08:24:12','2018-10-16 15:34:10'),('13','tạo bộ chứa mới','tao-bo-chua-moi','9','1','user_create','4','2018-10-16 04:31:33','2018-10-16 15:08:29'),('14','Tân','tan','9','1','user_create','3','2018-10-16 15:02:19','2018-10-16 15:08:29'),('15','TEST02','test02','9','1','user_create','6','2018-10-16 15:03:25','2018-10-16 15:08:29'),('16','tnaa','tnaa','9','1','user_create','2','2018-10-16 15:06:40','2018-10-16 15:08:29'),('17','Tan2','tan2','9','1','user_create','1','2018-10-16 15:06:55','2018-10-16 15:06:55'),('18','Tan','tan-1','9','1','user_create','3','2018-10-16 15:08:47','2018-10-16 15:09:16'),('19','Tantna','tantna','9','1','user_create','2','2018-10-16 15:08:49','2018-10-16 15:09:16'),('20','Tan','tan-2','9','1','user_create','3','2018-10-16 15:17:15','2018-10-16 15:34:10'),('21','Tantna','tantna-1','9','1','user_create','2','2018-10-16 15:17:20','2018-10-16 15:34:10'),('22','Đạt tạo bộ chứa','dat-tao-bo-chua','9','0','user_create','1','2018-10-16 21:24:14','2018-10-16 21:24:14');
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;

DROP TABLE IF EXISTS `task_label`;
CREATE TABLE `task_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `check` tinyint(4) NOT NULL DEFAULT '0',
  `pos` tinyint(4) DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `task_label` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_label` ENABLE KEYS */;

DROP TABLE IF EXISTS `user_in_group`;
CREATE TABLE `user_in_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `UserGroupId` int(11) NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `user_in_group` DISABLE KEYS */;
INSERT INTO `user_in_group` VALUES ('27','10','4','2'),('28','10','5','1'),('29','10','6','1'),('35','11','1','1'),('36','11','2','1'),('37','11','4','1'),('38','11','5','1'),('39','11','6','1'),('42','9','1','1'),('43','9','2','1');
/*!40000 ALTER TABLE `user_in_group` ENABLE KEYS */;

DROP TABLE IF EXISTS `usergroups`;
CREATE TABLE `usergroups` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `Description` text NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '1' COMMENT '1-Trưởng nhóm phòng ban 2 - Trưởng nhóm project',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
INSERT INTO `usergroups` VALUES ('1','Admin','1','Quản trị toàn bộ hệ thống.','1'),('2','IT help desk.','1','IT help desk. chỉ có quyền check log,  back, restore.','1'),('3','Phòng nhân sự','0','Phòng nhân sự','1'),('4','Phòng marketing','1','Phòng marketing','1'),('5','Phòng kinh doanh','1','Phòng kinh doanh','1'),('6','Phòng tài chính','1','Phòng tài chính','1'),('7','admin02','1','Quản trị hệ thống','1'),('8','test ui2','1','ok6','1');
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `Id` int(10) unsigned NOT NULL,
  `Fullname` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Avatar` text,
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `Note` mediumtext,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `UpdateTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `users_email_unique` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('7','Nguyễn Văn a','puma@gmail.com','','','1','qeweg','2018-10-04 11:51:08','2018-10-04 11:51:08'),('9','Nguyễn Việt Dinh','vdbkpro@gmail.com','1662727846','','0','','2018-10-04 11:01:36','2018-10-04 11:01:36'),('27','Khang Lê','khangle19@gmail.com','01662727846','','1','DEV','2018-10-04 15:09:10','2018-10-04 15:09:10'),('60','Nguyễn Tiến Đạt','tiendatbt1129@gmail.com','1662727846','','0','','2018-10-04 11:01:36','2018-10-04 11:01:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;


-- phpMiniAdmin dump end
