<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel 2</title>
        <base href="{{ url('') }}/">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            window.Laravel = {
                csrfToken: '{{ csrf_token() }}'
            };
            window.api_url = "{{ URL::to('/api/') }}";
            window.CORE_DOMAIN = "{{ config('constants.DOMAIN') }}";
            var UID = {
                _current: 0,
                getNew: function(){
                    this._current++;
                    return this._current;
                }
            };

            HTMLElement.prototype.pseudoStyle = function(element,prop,value){
                var _this = this;
                var _sheetId = "pseudoStyles";
                var _head = document.head || document.getElementsByTagName('head')[0];
                var _sheet = document.getElementById(_sheetId) || document.createElement('style');
                _sheet.id = _sheetId;
                var className = "pseudoStyle" + UID.getNew();
                
                _this.className +=  " "+className; 
                
                _sheet.innerHTML += " ."+className+":"+element+"{"+prop+":"+value+"}";
                _head.appendChild(_sheet);
                return this;
            };
        </script>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/fabric.min.css') }}">
        <!-- Font -->
        <link href="{{ asset('css/quicksand.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
        <script src="{{ URL::to('/lang.js') }}"></script>
    </head>
    <body>
        <div id="symper-app">
            <app></app>
        </div>
        <script src="{{ asset('js/app.js')}}?v={{time()}}"></script>
    </body>
</html>
