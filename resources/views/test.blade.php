<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <!-- BEGIN Pre-requisites -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>
        <!-- END Pre-requisites -->
        <script>
            function start() {
                gapi.load('auth2', function() {
                    auth2 = gapi.auth2.init({
                        client_id: 'Y205254581811-n94hmttje8fcqevifs9v7jp9dkms3qam.apps.googleusercontent.com',
                    });
                });
            }
        </script>
    </head>
    <body>
        <button id="signinButton">Sign in with Google</button>
        <script>
            $('#signinButton').click(function() {
            // signInCallback defined in step 6.
                auth2.grantOfflineAccess().then(signInCallback);
            });

            function signInCallback(authResult) {
                if (authResult['code']) {
                    // Hide the sign-in button now that the user is authorized, for example:
                    $('#signinButton').attr('style', 'display: none');
                    // Send the code to the server
                    $.ajax({
                        type: 'POST',
                        url: 'http://example.com/storeauthcode',
                        // Always include an `X-Requested-With` header in every AJAX request,
                        // to protect against CSRF attacks.
                        headers: {
                          'X-Requested-With': 'XMLHttpRequest'
                        },
                        contentType: 'application/octet-stream; charset=utf-8',
                        success: function(result) {
                            // Handle or verify the server response.
                        },
                        processData: false,
                        data: authResult['code']
                    });
                } else {
                    // There was an error.
                }
            }
        </script>
    </body>
</html>