<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['api']], function () {
    Route::get('/{group_id?}/{task_slug?}', function () {
        return view('index');
    })->where([
        'group_id' => "[A-Z0-9]{8}",
        'task_slug' => "[\w\-0-9]+",
    ]);

    Route::get('file/{file_id}', 'FilesController@download'); 
    Route::get('view/{file_id}', 'FilesController@view'); 
    Route::get('thumb/{file_id}', 'FilesController@thumbnail'); 
    Route::get('full/{file_id}', 'FilesController@view_full'); 
    Route::get('/mail', function () {
        return view('test');
    });

});
Route::get('lang.js', function(){
    $lang = config('app.locale');
    $files   = glob(resource_path('lang/' . $lang . '/*.php'));
    $string = [];

    foreach ($files as $file) {
        $name          = basename($file, '.php');
        $string[$name] = require $file;
    }
    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($string) . ';');
    exit();
}); 
Route::get('js/lang.js', function(){
    $lang = config('app.locale');
    $files   = glob(resource_path('lang/' . $lang . '/*.php'));
    $string = [];

    foreach ($files as $file) {
        $name          = basename($file, '.php');
        $string[$name] = require $file;
    }
    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($string) . ';');
    exit();
}); 
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});