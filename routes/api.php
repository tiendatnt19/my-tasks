<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('synce-users-from-core', 'SyncsController@synce_users_from_core');
Route::get('update-users-from-core', 'SyncsController@update_users_from_core');
Route::get('synce-usergroups-from-core', 'SyncsController@synce_usergroups_from_core');
Route::get('update-usergroups-from-core', 'SyncsController@update_usergroups_from_core');
Route::get('synce-user-in-group-from-core', 'SyncsController@synce_user_in_group_from_core');
Route::get('update-user-in-group-from-core', 'SyncsController@update_user_in_group_from_core');
Route::get('remove-user-in-group-from-core', 'SyncsController@remove_user_in_group_from_core');
Route::get('synce-task-from-core', 'SyncsController@synce_task_from_core');
Route::get('synce-task-from-event', 'SyncsController@synce_task_from_event');
Route::get('update-task-from-event', 'SyncsController@update_task_from_event');


Route::group(['middleware' => ['api']], function () {
    // Add note
    Route::post('add-note', 'NotesController@store');
    
    // Add plan
    Route::post('add-plan', 'GroupsController@add_plan');

    // Add plan
    Route::post('update-plan', 'GroupsController@update_plan');

    // Add plan member
    Route::post('add-plan-member', 'GroupsController@add_member_to_plan');

    // Add favorite plan
    Route::post('toggle-favorite-plan', 'GroupsController@toggle_favorite_plan');

    // Add favorite plan
    Route::post('toogle-plan-status', 'GroupsController@toogle_plan_status');

    // Remove plan member
    Route::post('remove-plan-member', 'GroupsController@remove_plan_member');

    // Remove plan
    Route::get('delete-plan/{plan_id}', 'GroupsController@delete_plan');
    
    // Delete note
    Route::get('delete-note/{note_id}', 'NotesController@destroy');
    
    // Get notes
    Route::get('get-notes/{group_id}', 'NotesController@index');
    Route::get('get-only-notes/{group_id}', 'NotesController@get_only_notes');

    // Get notes
    Route::get('get-plans', 'GroupsController@get_plans');

    // Get notes
    Route::get('get-favorites-plans', 'GroupsController@get_favorites_plans');

    // Get plan statistic
    Route::get('get-plan-statistic/{group_id}', 'GroupsController@get_plan_statistic');

    // Get plan statistic
    Route::get('get-core-plan-statistic/{group_id}', 'GroupsController@get_core_plan_statistic');

    // Update note
    Route::post('update-note', 'NotesController@update');

    // delete note
    Route::get('delete-note/{id}', 'NotesController@destroy');
    
    // Add card
    Route::post('add-card', 'CardsController@store');
    
    // Get card
    // Route::get('get-cards/{note_id}', 'CardsController@get_cards');
    
    // Add card
    Route::get('get-card-detail/{note_id}/{card_id}', 'CardsController@get_card_detail');
    
    // Update card
    Route::post('update-card', 'CardsController@update');

    // Update card
    Route::post('update-label', 'CardsController@update_label');

    // Update card
    Route::post('uncheck-labels', 'CardsController@uncheck_labels');

    // delete card
    Route::get('delete-card/{id}', 'CardsController@destroy');

    // Move card to note
    Route::post('move-card-to-note', 'CardsController@move_card_to_note');

    // Copy one task to other note without it content like file, comment, member, just remain checklist
    Route::post('copy-card-to-note', 'CardsController@copy_card_to_note');

    // Move all card to note
    Route::post('move-all-cards-to-note', 'CardsController@move_all_cards_to_note');

    // Add member to card
    Route::post('add-member-to-card', 'CardsController@add_member_to_card');

    // Add member to card
    Route::post('remove-card-member', 'CardsController@remove_card_member');

    Route::post('set-repeat-task', 'CardsController@set_repeat_task');

    // Add member to card
    Route::post('add-comment-to-card', 'CommentsController@store');
    
    // Get comments
    Route::get('get-comments/{card_id}', 'CommentsController@index');

    // Reply to a comment
    Route::post('reply-comment', 'CommentsController@reply_to_comment');
        
    // Remove a comment
    Route::post('remove-comment', 'CommentsController@destroy');

    // Add todo list
    Route::post('add-todo-list', 'ChecklistsController@store');
    
    // Check todo list
    Route::post('check-todo-list', 'ChecklistsController@check_todo_list');
        
    // Update todo list
    Route::post('update-todo-list', 'ChecklistsController@update');
        
    // Get todo list
    Route::get('get-todo-list', 'ChecklistsController@index');
        
    // Get todo list
    Route::get('remove-todo-list/{checklist_id}', 'ChecklistsController@destroy');
        
    // Add attachement
    Route::post('add-attachement', 'FilesController@store');

    Route::get('check-permission/{file_id}/{card_id}', 'FilesController@check_file_permission');
        
    // Move attachement
    Route::post('move-attachement-to-card', 'FilesController@move_attchement_to_card');
        
    // copy attachement
    Route::post('copy-attachement-to-card', 'FilesController@copy_attchement_to_card');
        
    // Get attachement
    Route::get('get-attachement/{card_id}', 'FilesController@index');

    // Get file setting
    Route::get('get-file-setting/{file_id}/{card_id}', 'FilesController@get_file_setting');
        
    // update attachement setting
    Route::post('update-file-setting', 'FilesController@update_user_can_see_file');
    Route::post('update-file-name', 'FilesController@update_file_name');
        
    // Delete attachement
    Route::get('delete-attachement/{file_id}', 'FilesController@delete_attchement');

    // Get users in selected group
    Route::get('get-users-in-group/{group_id}', 'CardsController@get_all_user_in_group');
    
    // Get all users
    Route::get('get-users', 'CardsController@get_users');

    // Get users
    Route::get('get-current-user', 'CardsController@get_current_user');
        
});