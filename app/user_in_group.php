<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
class user_in_group extends Model{
    public $table = 'user_in_group';
    public function is_user_in_group($group_id, $user_id){
        return $this->where([
            'UserId' => $user_id,
            'UserGroupId' => $group_id,
        ])->count();
    }

    public function is_admin_of_group($group_id, $user_id){
        return $this->where([
            'UserId' => $user_id,
            'UserGroupId' => $group_id,
            'Level' => 2
        ])->count();
    }

    public function add_member($group_id, $user_id){
        return $this->insert([
            'UserId' => $user_id,
            'UserGroupId' => $group_id,
            'Level' => 1
        ]);
    }
}
