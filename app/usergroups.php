<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
class usergroups extends Model{
    
    public function get_group_from_note($note_id){
        $group = $this->select("*")
            ->join("notes", 'notes.group_id', '=', 'usergroups.Id')
            ->where(DB::raw('notes.id'), '=', $note_id)
            ->first();
        if (empty($group)) {
            $group = (Object) [
                'Name' => __('template.my_board'),
                'Id' => 0,
                'Status' => 0,
                'from' => 'manual',
                'source_id' => 0
            ];
        }
        return $group;
    }
    public function get_favorites_groups($user_id){
        return $this->select("*")
    		->join("favorite_group", 'favorite_group.group_id', '=', 'usergroups.Id')
    		->where(DB::raw('favorite_group.user_id'), '=', $user_id)
	    	->get();
    }
}
