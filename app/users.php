<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
class users extends Model{
    public function get_users_in_group($group_id){
        return DB::table('user_in_group')
            ->join('users', 'users.Id', '=', 'user_in_group.UserId')
            ->where('user_in_group.UserGroupId', '=', $group_id)
            ->get();
    }

    public function get_groups_involved_in($user_id){
    	return DB::table('usergroups')->select("*")
    		->join(DB::raw("(SELECT DISTINCT UserGroupId, level FROM `user_in_group` WHERE UserId = $user_id) as G"), DB::raw('G.UserGroupId'), '=', 'usergroups.Id')
	    	->get();
    }

    public function add_user_in_group($data){
        return DB::table('user_in_group')->insert($data);
    }

    public function get_card_member($card_id){
        return $this->select('users.*')
        	->join('card_members', 'card_members.member_id', '=', 'users.Id')
        	->where(['card_members.card_id' => $card_id])->get();
    }

    public function get_my_staff($uid){
        // Lấy toàn bộ group mà người dùng làm admin
        $groupsId = DB::table('usergroups')->select("usergroups.Id")
            ->join(DB::raw("(SELECT DISTINCT UserGroupId FROM `user_in_group` WHERE level = 2 AND UserId = $uid) as G"), DB::raw('G.UserGroupId'), '=', 'usergroups.Id')
            ->get();
        $ids = [];
        foreach ($groupsId as $id) {
            $ids[] = $id->Id;
        }
        return DB::table('user_in_group')->select('users.*')->distinct('Id')
            ->join('users', 'users.Id', '=', 'user_in_group.UserId')
            ->whereIn('user_in_group.UserGroupId', $ids)
            ->where('user_in_group.UserId', '<>', $uid)
            ->get();

    }
}
