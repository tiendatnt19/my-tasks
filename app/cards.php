<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\card_members;
use App\usergroups;
use App\checklists;
use App\card_labels;
use App\comments;
use App\files;
use App\users;
use DB;
class cards extends Model{
    public function is_a_card_member($card_id, $uid){
        $card_member = new card_members;
        return count($card_member->where([
                'member_id' => $uid,
                'card_id' => $card_id
            ])
            ->distinct('member_id')
            ->get()
        );
    }

    public function get_plan_synce_cards($card_ids, $uid){
        return $this->select('cards.*')
            ->join('card_members', 'card_members.card_id', '=', 'cards.id')
            ->where([
                'card_members.member_id' => $uid 
            ])
            ->whereNotIn('card_members.card_id', $card_ids)
            ->get();
    }

    public function is_a_note_member($uid, $note_id){
        return count($this->select('card_members.member_id')
            ->join('card_members', 'card_members.card_id', '=', 'cards.id')
            ->where([
                'cards.note_id' => $note_id,
                'card_members.member_id' => $uid 
            ])
            ->distinct('card_members.member_id')
            ->get()
        );
    }

    public function get_card($card_id, $current_uid){
        $files = new files;
        $usergroups = new usergroups;
        $users = new users;
        $label = new card_labels;
        $card = $this->where(['id' => $card_id])->first();
        $card->group = $this->get_group_from_note($card->note_id);
        $card->files = $files->get_files_for_card($card_id);
        $card->checklists = checklists::where(['card_id' => $card_id])->orderBy('id', 'asc')->get();
        $card->comments = comments::where(['card_id' => $card_id, 'parent_id' => 0])->orderBy('id', 'desc')->get();
        $card->members = $users->get_card_member($card_id);
        $card->user = $current_uid;
        $card->link = url(en_id($card->group->Id).'/'.$card->card_slug);
        return $card;
    }

    public function get_group_from_note($note_id){
        $group = DB::table('usergroups')->select("*")
    		->join("notes", 'notes.group_id', '=', 'usergroups.Id')
    		->where(DB::raw('notes.id'), '=', $note_id)
	    	->first();
    	if (empty($group)) {
    		$group = (Object) [
    			'Name' => __('template.my_board'),
    			'Id' => 0,
    			'Status' => 0,
    			'from' => 'manual',
    			'source_id' => 0
    		];
    	}
    	return $group;
    }

    public function get_max_position_of($note_id){
        return $this->select( DB::raw('max(position) as max_position') )->where(['note_id' => $note_id])->first()->max_position;
    }

    public function get_user_cards($uid){
        return $this->select('cards.*')->join('card_members', 'card_members.card_id', '=', 'cards.id')
            ->where('card_members.member_id', '=', $uid)->get();
    }
    
}
