<?php 
if (!function_exists('remove_vn_accents')) {
    # code...
    function remove_vn_accents($text){
        $text = str_replace('/', '-', $text);
        $text = str_replace('"', '', $text);
        $text = str_replace('\'', '', $text);
        $utf8 = array(
            '/[áàâãăạảắẳẵằặấầẩẫậ]/u' => 'a',
            '/[íìịĩỉ]/u' => 'i',
            '/[éèêẹẽếềễệẻể]/u' => 'e',
            '/[óòôõọỏơờởớợỡồổốộ]/u' => 'o',
            '/[úùũụủưứừửữự]/u' => 'u',
            '/[ýỹỷỳỵ]/u' => 'y',
            '/[đĐ]/u' => 'd',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => '', // Literally a single quote
            '/[“”«»„]/u' => '', // Double quote
            '/[ ]/u' => '-', // Double quote
            '/[\/]/u' => '', // Double quote
        );
        $text = preg_replace(array_keys($utf8), array_values($utf8), $text);
        $replace = array(
            'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-',
            'Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae',
            'Þ'=>'B',
            'Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C',
            'È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E',
            'Ğ'=>'G',
            'İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I',
            'Ł'=>'L',
            'Ñ'=>'N', 'Ń'=>'N',
            'Ø'=>'O', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe',
            'Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S',
            'Ț'=>'T',
            'Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue',
            'Ý'=>'Y',
            'Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z',
            'â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae',
            'б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
            'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch',
            'ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 'д'=>'d', 'Д'=>'D', 'ð'=>'d',
            'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
            'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f',
            'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
            'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
            'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
            'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
            'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
            'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
            'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
            'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
            'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe',
            'פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
            'ק'=>'q',
            'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
            'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
            'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
            'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
            'в'=>'v', 'ו'=>'v', 'В'=>'v',
            'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
            'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
            'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
        );
        $text = strtr($text, $replace);
        $text = preg_replace("/[^a-zA-Z0-9-]/", "", $text);
        return strtolower($text);
    }
}

if (!function_exists('en_id')) {
    # code...
    /*
    * en - de : code id
    */
    function en_id($id){
        // return $id;
        $id = intval($id) * Config::get('constants.ENCODE_2');
        $id = dechex($id + Config::get('constants.ENCODE_1'));
        $id = str_replace(1, 'J', $id);
        $id = str_replace(6, 'V', $id);
        $id = str_replace(8, 'S', $id);
        $id = str_replace(9, 'R', $id);
        $id = str_replace(7, 'T', $id);
        return strtoupper($id);
    }
}
if(!function_exists('de_id')){
    function de_id($id){
        // return $id;
        $id = str_replace('J', 1, $id);
        $id = str_replace('V', 6, $id);
        $id = str_replace('S', 8, $id);
        $id = str_replace('R', 9, $id);
        $id = str_replace('T', 7, $id);
        $id = (hexdec($id) - Config::get('constants.ENCODE_1')) / Config::get('constants.ENCODE_2');
        return $id;
    }
}

if (!function_exists('send_json')) {
    function send_json($data){
        header('Content-Type: application/json;charset=utf-8');
        $json = json_encode($data, JSON_PRETTY_PRINT);
        if ($json === false) {
            $json = json_encode(convert_from_latin1_to_utf8_recursively($data));
            if ($json === false) {
                $json = '{"jsonError": "unknown"}';
            }
            http_response_code(500);
        }
        die($json);
    }
}
if(!function_exists('check_valid_email')){
    function check_valid_email($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}

if(!function_exists('check_valid_phone_number')){

    function check_valid_phone_number($phonenumber){
        return preg_match('/^0(3|9|8)\d{8}$/', $phonenumber);
    }
}

if(!function_exists('check_valid_password')){
    function check_valid_password($password){
        return preg_match('/^(?=.*[A-Z])(?=.*[!@#_])(?=.*[0-9])(?=.*[a-z]).{8,30}$/', $password);
    }
}

if(!function_exists('check_valid_username')){
    function check_valid_username($username){
        return preg_match('/^[a-zA-Z0-9_]{8,30}$/', $username);
    }
}

if(!function_exists('check_valid_fullname')){

    function check_valid_fullname($fullname){
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z ]{8,30}$/', $fullname);
    }
}

if(!function_exists('check_valid_sirname')){
    function check_valid_sirname($sirname){
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z ]{2,25}$/', $sirname);
    }
}
if(!function_exists('check_valid_slug')){
    function check_valid_slug($slug){
        return preg_match('/^[a-zA-Z0-9_-]{3,255}$/', $slug);
    }
}

if(!function_exists('check_valid_datetime')){
    function check_valid_datetime($date){
        return DateTime::createFromFormat('Y-m-d H:i:s', $date);
    }
}

if (!function_exists('send_message_activeMQ')) {
    function send_message_activeMQ($topic,$data){
        $stomp = new Stomp("tcp://localhost:61613");
        $stomp->send("/topic/".$topic,json_encode($data));
    }
}
if (!function_exists('CURL_REQUEST')) {
    function CURL_REQUEST($url, $method = 'GET', $data = array(), $cookie = false, $timeout = 60){
        try {
            $ch = curl_init();
            $time_start = microtime(true);
            if (isset($cookie) && $cookie != '') {
                curl_setopt($ch, CURLOPT_COOKIE, $cookie);
            }
            //var_dump($cookie);
            if ($method == 'POST') {
                curl_setopt($ch, CURLOPT_POST, 1);
                if (is_array($data) && sizeof($data) == 0) {
                    $data['params_default_324324'] = 1;
                }
            }

            if ($method == 'DELETE') {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            }

            if (sizeof($data) > 0) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            }

            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $domain = base_url;
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin: '.$domain, 'Referer: '.$domain));
            // curl_setopt($ch, CURLOPT_USERAGENT, $headerinfo['User-Agent']);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $rs = curl_exec($ch);
            curl_close($ch);

            return $rs;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

?>