<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cards;
use App\notes;
use App\checklists;
use App\card_members;
use App\card_labels;
use App\card_files;
use App\comments;
use App\files;
use App\users;
use App\usergroups;
use App\repeat_task;
use App\logs;
use App\user_in_group;
use App\Http\Resources\Cards as CardsResource;
use App\Http\Resources\Files as FilesResource;
use App\Http\Resources\Comments as CommentsResource;
use App\Http\Resources\Checklists as ChecklistsResource;
use App\Http\Resources\Users as UsersResource;
use App\Http\Resources\Card_labels as LabelResource;
use App\Http\Resources\Logs as LogResource;

class CardsController extends Controller{
    public function __construct(){
        $this->note_model = new notes;
        $this->card_model = new cards;
        $this->checklist_model = new checklists;
        $this->comment_model = new comments;
        $this->file_model = new files;
        $this->user_model = new users;
        $this->label_model = new card_labels;
        $this->user_group_model = new user_in_group;
        $this->groups_model = new usergroups;
        $this->logs_model = new logs;
        $this->card_files_model = new card_files;
        $this->schedule_model = new repeat_task;
    }

    public function get_card_detail(Request $request, $note_id, $card_id){
        $note_id = de_id($note_id);
        $note_to_get = $this->note_model->where([
            'id' => $note_id
        ])->first();
        if(empty($note_to_get)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        $card_id = de_id($card_id);
        $card = $this->card_model
            ->where([
                'note_id' => $note_id,
                'id' => $card_id,
            ])
            ->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        // Kiểm tra ng dùng có phải là thành viên của tác vụ, ng tạo ra bộ chứa hay là thành viên của kế hoạch
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            (int) $user['uid'] != (int) $note_to_get->owner_id && 
            !$this->user_group_model->is_user_in_group($note_to_get->group_id, $user['uid']) &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        
        $card->todolist = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $card_id])->orderBy('id', 'asc')->paginate(50));
        $comment_data = $this->comment_model->where(['card_id' => $card_id, 'parent_id' => 0])->orderBy('id', 'desc')->get();
        foreach ($comment_data as &$cmt) {
            $file = $this->file_model->select('files.*')
                ->join('comment_files', 'comment_files.file_id', '=', 'files.id')
                ->where(['comment_files.comment_id' => $cmt->id])
                ->first();
            if (!empty($file)) {
                $cmt->file = [
                    'file_id' => en_id($file->id),
                    'file_name' => $file->file_name,
                    'file_type' => $file->file_type,
                    'file_url' => $file->file_url,
                    'file_size' => $file->file_size,
                    'file_content_type' => $file->file_content_type
                ];
            }else{
                $cmt->file = null;
            }
            $cmt->replies = CommentsResource::collection(
                $this->comment_model->where([
                    'card_id' => $card_id, 
                    'parent_id' => $cmt->id
                ])
                ->orderBy('id', 'asc')->get()
            );
            foreach ($cmt->replies as &$reply) {
                $rep_file = $this->file_model->select('files.*')
                ->join('comment_files', 'comment_files.file_id', '=', 'files.id')
                ->where(['comment_files.comment_id' => $reply->id])
                ->first();
                if (!empty($rep_file)) {
                    $reply->file = [
                        'file_id' => en_id($rep_file->id),
                        'file_name' => $rep_file->file_name,
                        'file_type' => $rep_file->file_type,
                        'file_url' => $rep_file->file_url,
                        'file_size' => $rep_file->file_size,
                        'file_content_type' => $rep_file->file_content_type
                    ];
                }else{
                    $reply->file = null;
                }
            }
        }
        $card->comments = CommentsResource::collection($comment_data);
        $card->files = FilesResource::collection($this->file_model->get_files_for_card($card_id));
        $card->members = UsersResource::collection($this->user_model->get_card_member($card_id));
        $card->labels = LabelResource::collection($this->label_model->where(['card_id' => $card_id])->take(6)->get());
        $card->activities = LogResource::collection($this->logs_model->select('logs.*', 'users.Fullname')->join('users', 'logs.user_id', '=', 'users.Id')->where(['logs.card_id' => $card_id])->orderBy('created_at', 'desc')->get());
        $card->card_id = en_id($card_id);
        $card->note_id = en_id($note_id);
        $card->created_by = en_id($card->created_by);
        $schedule = $this->schedule_model->select('value')->where(['card_id' => $card_id, 'user_id' => $user['uid']])->first();
        $card->schedule = empty($schedule) ? '' : $schedule->value;
        unset($card->id);
        send_json([
            'status' => 1,
            'data' => $card
        ]);
    }

    public function update_label(Request $request){
        $card_id = de_id($request->card_id);
        $card = $this->card_model
            ->where([
                'id' => $card_id
            ])
            ->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $user = $request->get('user');
        $note_of_card = $this->note_model->where(['id' => $card->note_id])->first();
        // Kiểm tra ng dùng xem có thuộc tác vụ, ng tạo bộ chưa hay ng tạo ra kế hoạch không
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            (int) $user['uid'] !== (int) $card->created_by && 
            !$this->user_group_model->is_admin_of_group($note_of_card->group_id, $user['uid']) &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        // Kiểm tra biến
        $label_id = $request->label_id;
        if (empty($label_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'nhãn dán'])
            ]);
        }
        $label_id = de_id($label_id);
        $label = $this->label_model->where(['id' => $label_id])->first();
        if (empty($label)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.label_does_not_exist'),
                'id' => $label
            ]);
        }
        $field = $request->field;
        if (empty($field)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'trường để cập nhật'])
            ]);
        }
        if (!in_array($field, ['label_name', 'check'])) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.field_not_valid')
            ]);
        }
        $val = $request->val;
        if (!isset($val)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'value'])
            ]);
        }
        if ($field == 'check' && ( !is_numeric($val) || (int) $val > 1 || (int) $val < 0) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.value_not_valid')
            ]);
        }
        // Đã check xong, tiến hành sửa đổi
        if ($this->label_model->where(['id' => $label_id])->update([$field => $val])) {
            send_json([
                'status' => 1,
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $card_id,
                    'type' => config("constants.UPDATE_LABEL"),
                    'new_value' => $val,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_update_label')
            ]);
        }
    }

    public function uncheck_labels(Request $request){
        $card_id = de_id($request->card_id);
        $card = $this->card_model
            ->where([
                'id' => $card_id
            ])
            ->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $user = $request->get('user');
        $note_of_card = $this->note_model->where(['id' => $card->note_id])->first();
        // Kiểm tra ng dùng xem có thuộc tác vụ, ng tạo bộ chưa hay ng tạo ra kế hoạch không
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            (int) $user['uid'] !== (int) $card->created_by && 
            !$this->user_group_model->is_admin_of_group($note_of_card->group_id, $user['uid']) &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        
        // Đã check xong, tiến hành sửa đổi
        if ($this->label_model->where(['card_id' => $card_id])->update(['check' => 0])) {
            send_json([
                'status' => 1,
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $card_id,
                    'type' => config("constants.UPDATE_LABEL"),
                    'new_value' => 0,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_update_label')
            ]);
        }
    } 

    public function update(Request $request){
        
        if(empty($request->note_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'bộ chứa'])
            ]);
        }
        if(empty($request->card_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tác vụ'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $note_id = de_id($request->note_id);
        $card_to_update = $this->card_model->where([
            'id' => $card_id,
            'note_id' => $note_id
        ])->first();
        // kiểm tra tác vụ có tồn tại hay không
        if(empty($card_to_update)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card_to_update->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $data = [];
        $data['note_id'] = $note_id;
        $note_to_update = $this->note_model->where([
            'id' => $note_id
        ])->first();
        $user = $request->get('user');
        $send_queue = true;
        // kiểm tra xem user có quyền cập nhật tác vụ hay không
        if(!$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            $note_to_update->owner_id != $user['uid'] &&
            !$this->user_group_model->is_admin_of_group($note_to_update->group_id, $user['uid']) &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_edit_card')
            ]);
        }
        // Tạo slug mới khi cập nhật title
        if(!empty($request->card_title)){
            $data['card_title'] = $request->card_title;
            // tạo slug cho tác vụ - sau này có thể truy cập qua slug như id của tác vụ
            $data['card_slug'] = remove_vn_accents($data['card_title']); 
            $i = 1;
            while( count( $this->card_model->where([
                [ 'card_slug', '=', $data['card_slug'] ],
                [ 'id', '<>', de_id($request->card_id) ]
            ])->get() ) ){
                $data['card_slug'] = remove_vn_accents($data['card_title']) . '-' . $i++;
            }
            $log_data = [
                'user_id' => $user['uid'],
                'card_id' => $card_id,
                'type' => config("constants.UPDATE_TITLE"),
                'new_value' => $request->card_title,
            ];
        }
        if(!empty($request->card_des)){
            $data['card_des'] = $request->card_des;
            $log_data = [
                'user_id' => $user['uid'],
                'card_id' => $card_id,
                'type' => config("constants.UPDATE_DES"),
                'new_value' => $request->card_des,
            ];
        }
        if(isset($request->status)){
            $request->status = (int) $request->status;
            if( !is_int($request->status) || $request->status > 2 || $request->status < 0){
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.invalid_status')
                ]);
            }
            $data['status'] = $request->status;
            if ($request->status == 2) {
                $data['finish_by'] = $user['uid'];
            }else{
                $data['finish_by'] = 0;
            }
            $log_data = [
                'user_id' => $user['uid'],
                'card_id' => $card_id,
                'type' => config("constants.UPDATE_STATUS"),
                'new_value' => $request->status,
            ];
        }
        if (!empty($request->start_date) ) {
            // kiểm tra tính hợp lệ của thời hạn của tác vụ (nếu có)
            if (!check_valid_datetime($request->start_date)) { 
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.datetime_not_valid')
                ]);
            }
            $data['start_date'] = $request->start_date;
            $log_data = [
                'user_id' => $user['uid'],
                'card_id' => $card_id,
                'type' => config("constants.UPDATE_START_DATE"),
                'new_value' => $request->start_date,
            ];
        }
        if (!empty($request->expiry_date) ) {
            if (!check_valid_datetime($request->expiry_date)) { 
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.datetime_not_valid')
                ]);
            }
            $data['expiry_date'] = $request->expiry_date;
            $log_data = [
                'user_id' => $user['uid'],
                'card_id' => $card_id,
                'type' => config("constants.UPDATE_EXPIRY_DATE"),
                'new_value' => $request->expiry_date,
            ];
        }
        if(isset($request->position)){
            $send_queue = false;
            $data['position'] = (int) $request->position;
        } 
        if(isset($request->show_des)){
            $send_queue = false;
            $data['show_des'] = (int) $request->show_des;
        } 
        if(isset($request->is_urgent)){
            $data['is_urgent'] = (int) $request->is_urgent;
            $log_data = [
                'user_id' => $user['uid'],
                'card_id' => $card_id,
                'type' => config("constants.SET_URGENT"),
                'new_value' => $request->is_urgent,
            ];
        } 
        if(isset($request->show_checklist)){
            $send_queue = false;
            $data['show_checklist'] = $request->show_checklist;
        } 
        // lưu tác vụ và gán user tạo là thành viên của tác vụ
        if($this->card_model->whereId($card_id)->update($data)){ 
            $logs_id = en_id(0);
            if (isset($log_data) && !empty($log_data)) {
                $logs_id = en_id($this->logs_model->insertGetId($log_data));
            }
            $card_to_pop = $this->card_model->get_card( $card_id, $user['uid'] );
            if ($send_queue) {
                send_message_activeMQ(
                    config('topic.task.update'),
                    $card_to_pop
                );
            }
            send_json([
                'status' => 1,
                'acti_id' => $logs_id
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_update_card')
            ]);
        }
    }

    public function store(Request $request){
        $this->card_model->card_title = $request->input('card_title');
        // kiểm tra tiêu đề của thẻ
        if (empty($this->card_model->card_title)) { 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tiêu đề'])
            ]);
        }
        // tạo slug cho tác vụ - sau này có thể truy cập qua slug như id của tác vụ
        $card_slug = remove_vn_accents($this->card_model->card_title); 
        $i = 1;
        while( count( $this->card_model->where('card_slug', $card_slug)->get() ) ){
            $card_slug = remove_vn_accents($this->card_model->card_title) . '-' . $i++;
        }
        $this->card_model->card_slug = $card_slug;
        $note_id = de_id($request->note_id);
        $this->note_model = new notes;
        $note_to_add = $this->note_model->where([
            'id' => $note_id
        ])->first();
        // kiểm tra bộ chứa có tồn tại hay không
        if(empty($note_to_add)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }

        $this->card_model->note_id = $note_id;
        $user = $request->get('user');
        $this->card_model->created_by = $user['uid'];
        $this->card_model->status = 0;
        // kiểm tra xem user có quyền tạo bộ chứa hay không
        if(!$this->card_model->is_a_note_member($user['uid'], $note_id) && 
            $note_to_add->owner_id != $user['uid'] &&
            !$this->user_group_model->is_user_in_group($note_to_add->group_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_create_card')
            ]);
        }
        // kiểm tra tính hợp lệ của thời hạn của tác vụ (nếu có)
        if (!empty($request->start_date && !check_valid_datetime($request->start_date))) { 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.datetime_not_valid')
            ]);
        }
        if (!empty($request->expiry_date && !check_valid_datetime($request->expiry_date))) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.datetime_not_valid')
            ]);
        }
        // Tạo các giá trị mặc định
        $max_position = $this->card_model->get_max_position_of($this->card_model->note_id);
        $this->card_model->position = $max_position+1;
        $this->card_model->start_date = empty($request->start_date) ? null : $request->start_date;
        $this->card_model->expiry_date = empty($request->expiry_date) ? null : $request->expiry_date;
        // lưu tác vụ và gán user tạo là thành viên của tác vụ
        if($this->card_model->save()){
            // Thêm thành viên vào tác vụ
            $card_member = new card_members;
            if (!empty($request->members)) {
                foreach ($request->members as $value) {
                    // if ($user['uid'] == de_id($value)) {
                    //     continue;
                    // }
                    $card_member->insert([
                        'card_id' => $this->card_model->id,
                        'member_id' => de_id($value)
                    ]);
                }
            }

            // Tạo label cho tác vụ
            for ($i=1; $i < 7; $i++) { 
                $this->label_model->insert([
                    'card_id' => $this->card_model->id,
                    'label_name' => '',
                    'check' => 0,
                    'label_pos' => $i
                ]);
            }
            $this->card_model->source_id = $this->card_model->id;
            $this->card_model->save();
            
            // Thêm chính người tạo là thành viên của tác vụ
            // $card_member->card_id = $this->card_model->id;
            // $card_member->member_id = $user['uid'];
            // $card_member->save();
            $this->card_model->todolist = [];
            $this->card_model->totalComment = 0;

            /**
             * Push content lên Active MQ
             */
            $card_inserted = $this->card_model->where(['card_slug' => $card_slug])->first();
            $card = $this->card_model->get_card($card_inserted->id, $user['uid']);
            send_message_activeMQ(
                config('topic.task.insert'),
                $card
            );
            
            // Lấy thông tin để trả về
            $this->card_model->todolist = [];
            $this->card_model->totalComment = 0;
            $this->card_model->members = UsersResource::collection($this->user_model->get_card_member($this->card_model->id));;
            $this->card_model->files = [];
            $this->card_model->labels = LabelResource::collection($this->label_model->where(['card_id' => $this->card_model->id])->get());
            $this->card_model->schedule = '';
            send_json([
                'status' => 1,
                'data' => new CardsResource($this->card_model)
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_card')
            ]);
        }
    }

    public function destroy(Request $request, $id){
        $user = $request->get('user');
        $card_to_delete = $this->card_model->where([
            'id' => de_id($id)
        ])->first();
        if(empty($card_to_delete)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card_to_delete->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $note_to_update = $this->note_model->where('id', '=', $card_to_delete->note_id)->first();
        // Chỉ đc xóa khi người dùng là ng tạo plan | người tạo tác vụ | ng tạo bộ chứa
        if( 
            (int) $user['uid'] !== (int) $card_to_delete->created_by && 
            (int) $user['uid'] !== (int) $note_to_update->owner_id && 
            $this->user_group_model->is_admin_of_group($note_to_update->group_id, $user['uid']) 
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $card_to_pop = $this->card_model->get_card(de_id($id), $user['uid']);
        if($this->card_model->whereId(de_id($id))->delete()){
            send_message_activeMQ(
                config('topic.task.remove'),
                $card_to_pop
            );
            $this->logs_model->where(['card_id' => de_id($id)])->delete();
            $this->comment_model->where(['card_id' => de_id($id)])->delete();
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_delete_card')
            ]);
        }
    }

    public function move_card_to_note(Request $request){
        if(empty($request->card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tác vụ'])
            ]);
        }
        if(empty($request->to_note)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'di chuyển tới bộ chứa'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $to_note = de_id($request->to_note);
        $user = $request->get('user');
        $note_to_move_in = $this->note_model->where(['id' => $to_note])->first();
        $card_to_move = $this->card_model->where(['id' => $card_id])->first();
        if(empty($note_to_move_in)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        if(empty($card_to_move)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card_to_move->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $current_note = $this->note_model->where('id', '=', $to_note)->first();
        if(empty($current_note)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        /**
         * Kiểm tra xem người dùng có thể di chuyển tác vụ này không
         * (Người dùng là người tạo tác vụ hoặc là member trong tác vụ hoặc là người tạo ra bộ chứa này hoặc người dùng là người tạo ra kế hoạch)
         */
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            (int) $user['uid'] !== (int) $current_note->owner_id && 
            !$this->user_group_model->is_admin_of_group($current_note->group_id, $user['uid'])  &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
       
        // Cuối cùng cũng check hòm hòm
        // Ơn giời
        if($this->card_model->whereId($card_id)->update(['note_id' => $to_note])){ 
            send_json([
                'status' => 1,
                'msg' => __('api_validation.move_card_success', ['title' => $note_to_move_in->title])
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_move_card')
            ]);
        }
    }
    
    public function copy_card_to_note(Request $request){
        if(empty($request->card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tác vụ'])
            ]);
        }
        if(empty($request->to_note)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'di chuyển tới bộ chứa'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $to_note = de_id($request->to_note);
        $title = $request->title;
        $user = $request->get('user');
        $note_to_move_in = $this->note_model->where(['id' => $to_note])->first();
        $card_to_move = $this->card_model->where(['id' => $card_id])->first();
        if(empty($note_to_move_in)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        if(empty($card_to_move)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card_to_move->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        if($card_to_move->note_id == $to_note){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.dupplicate_card', [
                    'note' => $note_to_move_in->title,
                    'card' => $card_to_move->card_title
                ])
            ]);
        }
        $current_note = $this->note_model->where(['id' => $card_to_move->note_id])->first();
        /**
         * Kiểm tra xem người dùng có thể di chuyển tác vụ này không
         * Người dùng là người tạo tác vụ 
         * hoặc là member trong tác vụ 
         * hoặc là người tạo ra bộ chứa này
         * hoặc là thành viên của kế hoạch
         */
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            (int) $user['uid'] !== (int) $current_note->owner_id &&
            !$this->user_group_model->is_user_in_group($current_note->group_id, $user['uid']) &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
                'data' => '1'
            ]);
        }

        // Cuối cùng cũng check hòm hòm
        $card = $card_to_move;
        $options = $request->options;
        $new_card = array(
            "action"        => $card->action ,
            "card_des"      => in_array("description", $options) ? $card->card_des : '',
            "card_title"    => empty($title) ? $card->card_title : $title,
            "created_at"    => $card->created_at,
            "created_by"    => $card->created_by,
            "expiry_date"   => in_array("expiry_date", $options) ? $card->expiry_date : null,
            "from_source"   => $card->from_source,
            "note_id"       => $to_note,
            "position"      => $this->card_model->max('position'),
            "show_checklist" => $card->show_checklist,
            "show_des"      => $card->show_des,
            "is_urgent"     => $card->is_urgent,
            "finish_by"     => 0,
            "source_id"     => $card->source_id,
            "start_date"    => $card->start_date,
            "status"        => in_array("status", $options) ? $card->status : 0,
            "updated_at"    => $card->updated_at,
        );
        $new_card['card_slug'] = remove_vn_accents($new_card['card_title']); 
        $i = 1;
        while( count( $this->card_model->where('card_slug', $new_card['card_slug'])->get() ) ){
            $new_card['card_slug'] = remove_vn_accents($new_card['card_title']) . '-' . $i++;
        }
        $id = $this->card_model->insertGetId($new_card);
        if($id){ 
            // Thêm thành viên
            if (in_array("assignment", $options)) {
                $card_member_model = new card_members;
                $members = $card_member_model->where(['card_id' => $id])->get();
                foreach ($members as $mem) {
                    $card_member_model->insert([
                        'member_id' => $mem->member_id,
                        'card_id' => $id
                    ]);
                }
            }
            // Thêm công việc
            if (in_array("todolist", $options)) {
                $todos = $this->checklist_model->where(['card_id' => $id])->get();
                foreach ($todos as $todo) {
                    $this->checklist_model->insert([
                        'card_id' => $id,
                        'content' => $todo->content,
                        'status' => $todo->status
                    ]);
                }
            }
            // Thêm tệp đính kèm
            if (in_array("files", $options)) {
                $files = $this->file_model->get_files_for_card($card_id);
                foreach ($files as $file) {
                    $this->card_files_model->insert([
                        "card_id" =>  $id,
                        "file_id" => $file->id
                    ]);
                }
            }
            // Thêm nhãn dán
            if (in_array("labels", $options)) {
                $labels = $this->label_model->where(['card_id' => $id])->take(6)->get();
                foreach ($labels as $label) {
                    $this->label_model->insert([
                        'card_id' => $id,
                        'label_name' => $label->label_name,
                        'check' => $label->check,
                        'label_pos' => $i
                    ]);
                }
            }else{
                for ($i=1; $i < 7; $i++) { 
                    $this->label_model->insert([
                        'card_id' => $id,
                        'label_name' => '',
                        'check' => 0,
                        'label_pos' => $i
                    ]);
                }
            }
            $new_card['id'] = $id;
            $new_card['todolist'] = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $id])->orderBy('id', 'asc')->paginate(50));
            $new_card['totalComment'] = 0;
            $new_card['members'] = UsersResource::collection( $this->user_model->get_card_member($id) );
            $new_card['files'] = FilesResource::collection($this->file_model->get_files_for_card($card_id));
            $new_card['group_id'] = $note_to_move_in->group_id;
            $new_card['labels'] = LabelResource::collection($this->label_model->where(['card_id' => $id])->take(6)->get());
            $new_card['schedule'] = [];
            send_json([
                'status' => 1,
                'data' => new CardsResource( (object) $new_card)
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_move_card')
            ]);
        }
    }

    public function move_all_cards_to_note(Request $request){
        if(empty($request->from_note)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'from note id'])
            ]);
        }
        if(empty($request->to_note)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'to note id'])
            ]);
        }
        $from_note = de_id($request->from_note);
        $to_note = de_id($request->to_note);
        $user = $request->get('user');
        $note_to_move_in = $this->note_model->where(['id' => $to_note])->first();
        $note_to_export = $this->note_model->where(['id' => $from_note])->first();
        if(empty($note_to_move_in)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        if(empty($note_to_export)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        
        if($to_note == $from_note){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.dupplicate_card', ['note'=> $note_to_move_in->title, 'card' => ''])
            ]);
        }
        /**
         * Kiểm tra xem người dùng có thể di chuyển các tác vụ này không
         * Người dùng là người tạo ra bộ chứa này
         * Người dùng là người tạo bộ chứa cần di chuyển đến
         * Hoặc người dùng là admin của nhóm
         */
        if(
            (int) $user['uid'] !== (int) $note_to_export->owner_id && 
            (int) $user['uid'] !== (int) $note_to_move_in->owner_id &&
            !$this->user_group_model->is_admin_of_group($note_to_export->group_id, $user['uid']) &&
            !$this->card_model->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        if($this->card_model->where(['note_id' => $from_note])->update(['note_id' => $to_note])){ 
            send_json([
                'status' => 1,
                'msg' => __('api_validation.move_card_success', ['title' => $note_to_move_in->title])
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_move_card')
            ]);
        }
    }

    public function add_member_to_card(Request $request){
        if(empty($request->card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tác vụ'])
            ]);
        }
        if(empty($request->member_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thành viên'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $member_id = de_id($request->member_id);
        $card_member_model = new card_members;
        $user = $request->get('user');
        $card_to_assign = $this->card_model->where(['id' => $card_id])->first();
        
        if(empty($card_to_assign)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card_to_assign->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $member = $this->user_model->where(['Id' => $member_id])->first();
        if (empty($member)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.user_does_not_exist')
            ]);
        }
        $current_note = $this->note_model->where(['id' => $card_to_assign->note_id])->first();
        // Nếu không phải là ng tạo tác vụ hoặc là chủ của bộ chứa hoặc là ng tạo ra kế hoạch
        if(
            $card_to_assign->created_by !== $user['uid'] && 
            $current_note->owner_id !== $user['uid'] && 
            !$this->user_group_model->is_user_in_group($current_note->group_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
                // 'data' => $this->user_group_model->is_admin_of_group($current_note->group_id, $user['uid'])
            ]);
        }

        // Đã có trong tác vụ
        if($this->card_model->is_a_card_member($card_id, $member_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.dupplicate_member', ['card' => $card_to_assign->card_title])
            ]);
        }

        $card_member_model->card_id = $card_id;
        $card_member_model->member_id = $member_id;

        if($card_member_model->save()){
            send_message_activeMQ(
                config('topic.task_member.insert'),
                [
                    'card_id' => $card_id,
                    'member_id' => $member_id
                ]
            );
                        
            send_json([
                'status' => 1,
                'msg' => __('api_validation.add_member_success', ['member' => $member->Fullname, 'card' => $card_to_assign->card_title]),
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $card_id,
                    'type' => config("constants.ASSIGN_MEMBER"),
                    'new_value' => $member->Fullname,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_member', ['member' => $member->Fullname, 'card' => $card_to_assign->card_title]) 
            ]);
        }
    }
    
    public function remove_card_member(Request $request){
        if(empty($request->card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tác vụ'])
            ]);
        }
        if(empty($request->member_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thành viên'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $member_id = de_id($request->member_id);
        $card_member_model = new card_members;
        $user = $request->get('user');
        $card_to_remove = $this->card_model->where(['id' => $card_id])->first();
        $member = $this->user_model->where(['Id' => $member_id])->first();
        if (empty($member)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.user_does_not_exist')
            ]);
        }
        if(empty($card_to_remove)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card_to_remove->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }
        $current_note = $this->note_model->where(['id' => $card_to_remove->note_id])->first();
        if(
            $card_to_remove->created_by !== $user['uid'] && 
            $current_note->owner_id !== $user['uid'] && 
            !$this->card_model->is_a_card_member($card_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        if(!$this->card_model->is_a_card_member($card_id, $member_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.not_found_member')
            ]);
        }
        if($card_member_model->where([
            'card_id' => $card_id,
            'member_id' => $member_id
        ])->delete()){
            send_message_activeMQ(
                config('topic.task_member.remove'),
                [
                    'card_id' => $card_id,
                    'member_id' => $member_id
                ]
            );
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_remove_member'),
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $card_id,
                    'type' => config("constants.REMOVE_MEMBER"),
                    'new_value' => $member->Fullname,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_remove_member') 
            ]);
        }
    }
    
    public function get_all_user_in_group(Request $request){
        $user = $request->get('user');
        if (isset($request->group_id)) {
            $group_id = $request->group_id;
            $group_id = de_id($group_id);
        }else{
            $group_id = 0;
        }
        send_json([
            'status' => 1,
            'data' => UsersResource::collection($this->user_model->get_users_in_group($group_id))
        ]);
    }

    public function get_users(Request $request){
        send_json([
            'status' => 1,
            'data' => UsersResource::collection($this->user_model->get())
        ]);
    }

    public function get_current_user(Request $request){
        send_json([
            'status' => 1,
            'data' => new UsersResource($request->get('user')['data']),
        ]);
    }

    public function set_repeat_task(Request $request){
        $user = $request->get('user');
        $card_id = de_id($request->card_id);
        $card = $this->card_model
            ->where(['id' => $card_id])
            ->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($card->from_source == 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action'),
            ]);
        }

        $schedule = $request->schedule;
        if (!empty($this->schedule_model->where(['card_id' => $card_id, 'user_id' => $user['uid']])->first())) {
            $updated = $this->schedule_model
                ->where(['card_id' => $card_id, 'user_id' => (int) $user['uid']])
                ->update(['value' => implode(',',$schedule)]);
            if($updated){
                send_json([
                    'status' => 1
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.fail_to_update_card')
                ]);
            }
        }else{
            if($this->schedule_model->insert([
                'card_id' => $card_id, 
                'user_id' => $user['uid'],
                'value'   => implode(',',$schedule)
            ])){
                send_json([
                    'status' => 1
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.fail_to_add_card')
                ]);
            }
        }
    }
}
