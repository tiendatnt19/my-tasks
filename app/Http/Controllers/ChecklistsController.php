<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cards;
use App\notes;
use App\logs;
use App\checklists;
use App\Http\Resources\Checklists as ChecklistsResource;

class ChecklistsController extends Controller
{
    function __construct(){
        $this->card_model = new cards;
        $this->note_model = new notes;
        $this->logs_model = new logs;
        $this->checklist_model = new checklists;
    }

    
    public function index(Request $request){
        $card_id = $request->card_id;
        if(empty($card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($card_id);
        $card = $this->card_model->where(['id' => $card_id])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }

        $order = $request->order;
        if (empty($order)) {
            $order = 'asc';
        }

        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'id';
        }

        $data = $this->checklist_model->where(['card_id' => $card_id])->orderBy($sort, $order)->paginate(20);
        $total = $this->checklist_model->where(['card_id' => $card_id])->count();
        send_json([
            'status' => 1,
            'data' => ChecklistsResource::collection($data),
            'total' => $total
        ]);
    }
    
    public function store(Request $request){
         $card_id = $request->card_id;
        if(empty($card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($card_id);
        $card = $this->card_model->where(['id' => $card_id])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }

        $content = $request->content;
        if(empty($content)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'nội dung'])
            ]);
        }

        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) &&
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $this->checklist_model->card_id = $card_id;
        $this->checklist_model->content = $content;
        $this->checklist_model->status = 0;
        if($this->checklist_model->save()){
            
            send_json([
                'status' => 1,
                'data' => new ChecklistsResource($this->checklist_model),
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $card_id,
                    'type' => config("constants.ADD_TODOLIST"),
                    'new_value' => $content,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_comment')
            ]);
        }
    }

    public function check_todo_list(Request $request){
        if(empty($request->checklist_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'checklist'])
            ]);
        }

        $checklist_id = de_id($request->checklist_id);
        $checklist = $this->checklist_model->where(['id' => $checklist_id])->first();
        if(empty($checklist)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.checklist_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        $card = $this->card_model->where(['id' => $checklist->card_id ])->first();
        if(
            !$this->card_model->is_a_card_member($checklist->card_id, $user['uid']) &&
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        if($this->checklist_model->where([
            'id' => $checklist_id
        ])->update(['status' => $checklist->status ? 0 : 1])){
            
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_update_checklist', ['title' => $checklist->content]),
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $checklist->card_id,
                    'type' => config("constants.UPDATE_TODOLIST"),
                    'new_value' => $checklist->status ? 0 : 1,
                ]))
            ]);
        }else{
            send_json([
                'status' => 1,
                'msg' => __('api_validation.fail_to_update_checklist', ['title' => $checklist->content])
            ]);
        }
    }

    public function update(Request $request){
        if(empty($request->todolist_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'checklist'])
            ]);
        }

        $todolist_id = de_id($request->todolist_id);
        $checklist = $this->checklist_model->where(['id' => $todolist_id])->first();
        if(empty($checklist)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.checklist_does_not_exist')
            ]);
        }
        $content = $request->content;
        if(empty($content)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'nội dung'])
            ]);
        }
        $user = $request->get('user');
        $card = $this->card_model->where(['id' => $checklist->card_id ])->first();
        if(
            !$this->card_model->is_a_card_member($checklist->card_id, $user['uid']) &&
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        if($this->checklist_model->where([
            'id' => $todolist_id
        ])->update(['content' => $content])){
            
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_update_checklist', ['title' => $checklist->content]),
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $checklist->card_id,
                    'type' => config("constants.UPDATE_TODOLIST"),
                    'new_value' => $content,
                ]))
            ]);
        }else{
            send_json([
                'status' => 1,
                'msg' => __('api_validation.fail_to_update_checklist', ['title' => $checklist->content])
            ]);
        }
    }
    
    public function destroy(Request $request, $checklist_id){
        if(empty($checklist_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'checklist'])
            ]);
        }

        $checklist_id = de_id($checklist_id);
        $checklist = $this->checklist_model->where(['id' => $checklist_id])->first();
        if(empty($checklist)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.checklist_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        $card = $this->card_model->where(['id' => $checklist->card_id ])->first();
        if(
            !$this->card_model->is_a_card_member($checklist->card_id, $user['uid']) &&
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        if($this->checklist_model->where([
            'id' => $checklist_id
        ])->delete()){
            
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_remove_checklist', ['title' => $checklist->content]),
                'acti_id'=> en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $checklist->card_id,
                    'type' => config("constants.REMOVE_TODOLIST"),
                    'new_value' => $checklist->content,
                ]))
            ]);
        }else{
            send_json([
                'status' => 1,
                'msg' => __('api_validation.fail_to_remove_checklist', ['title' => $checklist->content])
            ]);
        }
    }
}
