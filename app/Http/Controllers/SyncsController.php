<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users;
use App\logs;
use App\cards;
use App\notes;
use App\Http\Resources\Users as UsersResource;
use Stomp;
use DateTime;
use mysqli;
class SyncsController extends Controller{
    public function __construct(){
        $this->user_model = new users;
        $this->log_model = new logs;
        $this->card_model = new cards;
        $this->note_model = new notes;
        $this->prefix = "symper_task";
        $this->servername = "localhost";
        $this->username = "taskdev";
        $this->password = "Symper@12345689";
        $this->dbname = "task_symper_vn";
    }

    public function synce_users_from_core(){
        $topic = config('topic.core.user.insert');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false && $frame !== NULL) {
                
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $Id = $data->Id;
                $FullName = $conn->real_escape_string($data->FullName);
                $Email = $conn->real_escape_string($data->Email);
                $Phone = $conn->real_escape_string($data->Phone);
                $Avatar = $conn->real_escape_string($data->Avatar);
                $Status = $data->Status;
                $Note = $conn->real_escape_string($data->Note);
                $CreateTime = $data->CreateTime;
                $UpdateTime = $data->UpdateTime;
                if (!DateTime::createFromFormat('Y-m-d H:i:s', $data->UpdateTime) || $data->UpdateTime == "0000-00-00 00:00:00" ) {
                    $UpdateTime = $data->CreateTime;
                }
                $sql = "SELECT * FROM users WHERE Email='$Email'";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {

                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO users (Id, FullName, Email, Phone, Avatar, Status, Note, CreateTime, UpdateTime) VALUES ($Id,'$FullName','$Email','$Phone','$Avatar',$Status,'$Note','$CreateTime','$UpdateTime')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE users SET FullName='$FullName',Phone='$Phone',Avatar='$Avatar',Status='$Status',Note='$Note',UpdateTime='$UpdateTime' WHERE Id=$Id";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                    continue;
                }
                $conn->close();
            }
        }
    }

    public function synce_usergroups_from_core(){
        $topic = config('topic.core.usergroup.insert');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $Id = $data->Id;
                $Name = $conn->real_escape_string($data->Name);
                $Status = $data->Status;
                $Description = $conn->real_escape_string($data->Description);
                $Type = $conn->real_escape_string($data->Type);
                $sql = "SELECT * FROM usergroups WHERE Name='$Name' AND source_id=$Id";
                $result = $conn->query($sql);
                
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO usergroups (`Name`, `Status`, `Description`, `Type`, `from`, `source_id`) VALUES ('$Name',$Status,'$Description','$Type', 'core', $Id)";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE usergroups SET Name='$Name',Status=$Status,Description='$Description',Type='$Type' WHERE source_id=$Id";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function update_user_in_group_from_core(){
        $topic = config('topic.core.usergroup.update');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $Id = $data->Id;
                $UsersId = $conn->real_escape_string($data->UsersId);
                $UsergroupId = $conn->real_escape_string($data->UsergroupId);
                $Level = $conn->real_escape_string($data->Level);
                $sql = "SELECT * FROM usergroups WHERE Name='$Name' AND source_id=$Id";
                $result = $conn->query($sql);
                
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO user_in_group (Id, UsersId, UsergroupId, Level) VALUES ($Id,'$UsersId',$UsergroupId,'$Level')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE user_in_group SET UsersId='$UsersId',UsergroupId=$UsergroupId,Level='$Level' WHERE Id=$Id";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function remove_user_in_group_from_core(){
        $topic = config('topic.core.usergroup.update');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $Id = $data->Id;
                $sql = "DELETE FROM usergroups WHERE source_id=$Id";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {
                    @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                }else{
                    $stomp->ack($frame);
                }
                $conn->close();
            }
        }
    }

    public function synce_user_in_group_from_core(){
        $topic = config('topic.core.user_in_group.insert');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                echo $frame->body;
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;

                $Id = $data->Id;
                $UserId = $data->UserId;
                $UserGroupId = $data->UserGroupId;
                $Level = $data->Level;
                $sql = "SELECT * FROM user_in_group WHERE source_id=$Id";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO user_in_group (Id, UserId, UserGroupId, Level) VALUES ($Id,'$UserId',$UserGroupId,'$Level')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE user_in_group SET UserId='$UserId',UserGroupId=$UserGroupId,Level='$Level' WHERE Id=$Id";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function update_usergroups_from_core(){
        $topic = config('topic.core.usergroup.update');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                echo $frame->body;
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $Id = $data->Id;
                $Name = $conn->real_escape_string($data->Name);
                $Status = $conn->real_escape_string($data->Status);
                $Description = $conn->real_escape_string($data->Description);
                $sql = "SELECT * FROM usergroups WHERE Name='$Name' AND source_id=$Id";
                $result = $conn->query($sql);
                
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO usergroups (Id, Name, Status, Description) VALUES ($Id,'$Name',$Status,'$Description')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE usergroups SET Name='$Name',Status=$Status,Description='$Description' WHERE Id=$Id";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function update_users_from_core(){
        $topic = config('topic.core.user.update');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(60);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $Id = $data->Id;
                $FullName = $conn->real_escape_string($data->FullName);
                $Email = $conn->real_escape_string($data->Email);
                $Phone = $conn->real_escape_string($data->Phone);
                $Avatar = $conn->real_escape_string($data->Avatar);
                $Status = $data->Status;
                $Note = $conn->real_escape_string($data->Note);
                $UpdateTime = $data->UpdateTime;
                if (!DateTime::createFromFormat('Y-m-d H:i:s', $data->UpdateTime) || $data->UpdateTime == "0000-00-00 00:00:00" ) {
                    $UpdateTime = date('Y-m-d H:i:s');
                }
                $sql = "SELECT * FROM users WHERE Email='$Email'";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO users (Id, FullName, Email, Phone, Avatar, Status, Note, CreateTime, UpdateTime) VALUES ($Id,'$FullName','$Email','$Phone','$Avatar',$Status,'$Note','$UpdateTime','$UpdateTime')";
                    if ($conn->multi_query ($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE users SET FullName='$FullName',Email='$Email',Phone='$Phone',Avatar='$Avatar',Status=$Status,Note='$Note',UpdateTime='$UpdateTime' WHERE Id=$Id";
                    if ($conn->multi_query ($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function synce_task_from_event(){
        $topic = config('topic.event.insert');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $from_source     = 'event';
                $source_id       = $data->id;
                $note_id         = $this->get_note_for_card($data->group_id, $data->manager_id);
                $card_title      = $conn->real_escape_string($data->Title);
                $card_slug       = $this->create_card_slug($data->Title);
                $card_des        = $conn->real_escape_string($data->Description);
                $created_by      = $data->manager_id;
                $status          = $data->status_id;
                $show_des        = 0;
                $show_checklist  = 0;
                $start_date      = date('Y-m-d H:i:s', strtotime($data->Start));
                $expiry_date     = date('Y-m-d H:i:s', strtotime($data->End));
                $position        = 1;
                $created_at      = $data->created_at;
                $updated_at      = $data->updated_at;
           
                $sql = "SELECT * FROM cards WHERE from_source='$from_source' AND source_id=$source_id;";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO cards (`card_title`, `card_slug`, `card_des`, `note_id`, `created_by`, `status`, `show_des`, `show_checklist`, `start_date`, `expiry_date`, `position`, `created_at`, `updated_at`, `from_source`, `source_id`) VALUES ('$card_title','$card_slug','$card_des', $note_id,'$created_by',$status,'$show_des','$show_checklist','$start_date', '$expiry_date', '$position', '$created_at', '$updated_at', '$from_source', '$source_id')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $card_id = $conn->insert_id;
                        $conn->query("INSERT INTO 'card_members' (`card_id`, `member_id`, `created_at`, `updated_at`) VALUES ($card_id, $created_by, '$created_at', '$updated_at')");
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE users SET `card_title`='$card_title',`card_slug`='$card_slug',`card_des`='$card_des',`created_by`='$created_by',`status`=$status,`show_des`='$show_des',`start_date`='$start_date', `expiry_date`='$expiry_date', `position`='$position', `updated_at`='$updated_at' WHERE `source_id`=$source_id AND `from_source`='$from_source'";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function update_task_from_event(){
        $topic = config('topic.event.update');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $from_source     = 'event';
                $source_id       = $data->id;
                $note_id         = $this->get_note_for_card($data->group_id, $data->manager_id);
                $card_title      = $conn->real_escape_string($data->title);
                $card_slug       = $this->create_card_slug($data->title);
                $card_des        = $conn->real_escape_string($data->description);
                $created_by      = $data->manager_id;
                $status          = $data->status_id;
                $show_des        = 0;
                $show_checklist  = 0;
                $start_date      = date('Y-m-d H:i:s', strtotime($data->start));
                $expiry_date     = date('Y-m-d H:i:s', strtotime($data->end));
                $position        = 1;
                $created_at      = $data->created_at;
                $updated_at      = $data->updated_at;
           
                $sql = "SELECT * FROM cards WHERE from_source='$from_source' AND source_id=$source_id;";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO cards (`card_title`, `card_slug`, `card_des`, `note_id`, `created_by`, `status`, `show_des`, `show_checklist`, `start_date`, `expiry_date`, `position`, `created_at`, `updated_at`, `from_source`, `source_id`) VALUES ('$card_title','$card_slug','$card_des', $note_id,'$created_by',$status,'$show_des','$show_checklist','$start_date', '$expiry_date', '$position', '$created_at', '$updated_at', '$from_source', '$source_id')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $card_id = $conn->insert_id;
                        $conn->query("INSERT INTO 'card_members' (`card_id`, `member_id`, `created_at`, `updated_at`) VALUES ($card_id, $created_by, '$created_at', '$updated_at')");
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE users SET `card_title`='$card_title',`card_slug`='$card_slug',`card_des`='$card_des',`created_by`='$created_by',`status`=$status,`show_des`='$show_des',`start_date`='$start_date', `expiry_date`='$expiry_date', `position`='$position', `updated_at`='$updated_at' WHERE `source_id`=$source_id AND `from_source`='$from_source'";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    public function synce_task_from_core(){
        $topic = config('topic.core.task.update');
        ignore_user_abort(true);
        set_time_limit(0);
        $stomp = new Stomp("tcp://localhost:61613", "", "", array("client-id" => $this->prefix.".$topic"));
        $stomp->subscribe("/topic/$topic",array("activemq.subscriptionName" => $this->prefix.".$topic"));
        $stomp->setReadTimeout(600);
        while (true) {
            $frame = $stomp->readFrame();
            if($frame !== false) {
                $data = json_decode($frame->body);
                $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname );
                if(!$conn){ continue; }
                $data = json_decode($frame->body);
                if(!is_object($data)) continue;
                $from_source     = 'core';
                $action          = $conn->real_escape_string($data->Action);
                $source_id       = $data->Id;
                $note_id         = $this->get_note_for_card(0, $data->UserId);
                $card_title      = $conn->real_escape_string($data->Title);
                $card_slug       = $this->create_card_slug($data->Title);
                $card_des        = $conn->real_escape_string($data->Description);
                $created_by      = $data->UserId;
                $status          = empty($data->Status) ? 0 : 1;
                $show_des        = 0;
                $show_checklist  = 0;
                $start_date      = date('Y-m-d H:i:s', strtotime($data->CreateTime));
                $expiry_date     = empty($data->DeadLine) ? "" : date('Y-m-d H:i:s', strtotime($data->DeadLine));
                $position        = 1;
                $created_at      = date('Y-m-d H:i:s', strtotime($data->CreateTime));
                $updated_at      = date('Y-m-d H:i:s', strtotime($data->CreateTime));
           
                $sql = "SELECT * FROM cards WHERE from_source='$from_source' AND source_id=$source_id;";
                $result = $conn->query($sql);
                if ($result == false || $result->num_rows == 0) {
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';INSERT INTO cards (`card_title`, `card_slug`, `card_des`, `note_id`, `created_by`, `status`, `show_des`, `show_checklist`, `start_date`, `expiry_date`, `position`, `created_at`, `updated_at`, `from_source`, `source_id`, `action`) VALUES ('$card_title','$card_slug','$card_des', $note_id,'$created_by',$status,'$show_des','$show_checklist','$start_date', '$expiry_date', '$position', '$created_at', '$updated_at', '$from_source', '$source_id', '$action')";
                    if ($conn->multi_query($sql) === TRUE) {
                        $card_id = $conn->insert_id;
                        $conn->query("INSERT INTO 'card_members' (`card_id`, `member_id`, `created_at`, `updated_at`) VALUES ($card_id, $created_by, '$created_at', '$updated_at')");
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }else{
                    $sql = "SET NAMES utf8;SET collation_connection='utf8_general_ci';UPDATE users SET `card_title`='$card_title',`card_slug`='$card_slug',`card_des`='$card_des',`created_by`='$created_by',`status`=$status,`show_des`='$show_des',`start_date`='$start_date', `expiry_date`='$expiry_date', `position`='$position', `updated_at`='$updated_at', action='$action' WHERE `source_id`=$source_id";
                    if ($conn->multi_query($sql) === TRUE) {
                        $stomp->ack($frame);
                    } else {
                        @file_put_contents('logs.txt', $topic. '-' . $frame->body.PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                $conn->close();
            }
        }
    }

    private function create_card_slug($title){
        $slug = remove_vn_accents($title); 
        $i = 1;
        while( count( $this->card_model->where('card_slug', $slug)->get() ) ){
            $slug = remove_vn_accents($title) . '-' . $i++;
        }
        return $slug;
    }

    private function get_note_for_card($group_id, $user_id){
        $note = $this->note_model->where(['type'=>'sync', 'group_id' => $group_id, 'owner_id' => $user_id])->first();
        if (!empty($note)) {
            return $note->id;
        }else{
            $this->note_model->title = "Công việc được đồng bộ";
            $this->note_model->slug = remove_vn_accents($this->note_model->title);
            $i = 1;
            while( count( $this->note_model->where('slug', $this->note_model->slug)->get() ) ){
                $this->note_model->slug = remove_vn_accents($this->note_model->title) . '-' . $i++;
            }
            $this->note_model->owner_id = $user_id;
            $this->note_model->group_id = $group_id;
            $this->note_model->type = 'sync';
            $this->note_model->position = 1;
            $this->note_model->save();
            return $this->note_model->id;

        }
    }

    public function run_all(){
        CURL_REQUEST('synce-users-from-core', 'GET');
        CURL_REQUEST('update-users-from-core', 'GET');
        CURL_REQUEST('synce-usergroups-from-core', 'GET');
        CURL_REQUEST('update-usergroups-from-core', 'GET');
        CURL_REQUEST('synce-user-in-group-from-core', 'GET');
        CURL_REQUEST('update-user-in-group-from-core', 'GET');
        CURL_REQUEST('remove-user-in-group-from-core', 'GET');
        CURL_REQUEST('synce-task-from-core', 'GET');
        CURL_REQUEST('synce-task-from-event', 'GET');
        CURL_REQUEST('update-task-from-event', 'GET');
    }

    private function check_topic_status($conn, $name){

    }
}
