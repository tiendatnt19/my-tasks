<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cards;
use App\notes;
use App\card_members;
use App\comments;
use App\users;
use App\files;
use App\comment_files;
use App\Http\Resources\Comments as CommentsResource;

class CommentsController extends Controller
{

    function __construct(){
        $this->card_model = new cards;
        $this->file_model = new files;
        $this->comment_files_model = new comment_files;
        $this->note_model = new notes;
        $this->comment_model = new comments;
        $this->user_model = new users;
    }
    
    
    public function index(Request $request, $card_id){
        // $card_id = $request->card_id;
        if(empty($card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($card_id);
        $card = $this->card_model->where(['id' => $card_id])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }

        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) ||
            !$this->card_model->is_a_note_member($user['uid'], $card->note_id) ||
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        $order = $request->order;
        if (empty($order)) {
            $order = 'desc';
        }

        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'id';
        }

        $data = $this->comment_model->where(['card_id' => $card_id, 'parent_id' => 0])->orderBy($sort, $order)->paginate(10);
        $total = $this->comment_model->where(['card_id' => $card_id, 'parent_id' => 0])->count();
        send_json([
            'status' => 1,
            'data' => CommentsResource::collection($data),
            'total' => $total
        ]);
    }

    public function store(Request $request){
        $card_id = $request->card_id;
        if(empty($card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($card_id);
        $card = $this->card_model->where(['id' => $card_id])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }

        $content = $request->content;
        if(empty($content)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'nội dung'])
            ]);
        }

        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) &&
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $this->comment_model->card_id = $card_id;
        $this->comment_model->content = $content;
        $this->comment_model->user_id = $user['uid'];
        if($this->comment_model->save()){
            $file = $request->file('attachement');
            if ($file != null) {
                $file_extension = $file->getClientOriginalExtension();
                $path = $file->store('attachements');
                $file_name = $file->getClientOriginalName();
                $file_name = explode('.', $file_name);
                array_pop($file_name);
                $file_name = implode('.', $file_name );
                $file_name = remove_vn_accents($file_name);
                $this->file_model->file_name = $file_name;
                $this->file_model->file_type = $file_extension;
                $this->file_model->file_url = $path;
                $this->file_model->file_size = $file->getClientSize();
                $this->file_model->file_content_type = $file->getMimeType();
                $this->file_model->author = $user['uid'];
                if ($this->file_model->save()) {
                    $this->comment_files_model->comment_id = $this->comment_model->id;
                    $this->comment_files_model->file_id = $this->file_model->id;
                    $this->comment_files_model->save();
                }
                $this->comment_model->file = [
                    'file_id' => en_id($this->file_model->id),
                    'file_name' => $file_name,
                    'file_type' => $file_extension,
                    'file_url' => "file/".en_id($this->file_model->id),
                    'file_size' => $file->getClientSize(),
                    'file_content_type' => $file->getMimeType(),
                ];
            }
            $card_to_pop = $this->card_model->get_card( $card_id, $user['uid'] );
            $mentions = [];
            foreach (explode(' ', $content) as &$word) {
                if ($word[0] == '@') {
                    $mentions[] = substr($word, 1);
                }
            }
            $mentions_uid = $this->user_model->select('Id')->whereIn('Email', $mentions)->get();
            send_message_activeMQ(
                config('topic.task_comment.insert'),
                [
                    'card_id'   => $card_id,
                    'content'   => $content,
                    'user'      => $user['uid'],
                    'created_by'=> $card_to_pop->created_by,
                    'link'      => $card_to_pop->link,
                    'members'   => $card_to_pop->members,
                    'mentions'  => $mentions_uid
                ]
            );
            $this->comment_model->replies = [];
            send_json([
                'status' => 1,
                'data' => new CommentsResource($this->comment_model),
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_comment')
            ]);
        }
    }

    public function reply_to_comment(Request $request){
        if(empty($request->card_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        if(empty($request->comment_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'bình luận'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $comment_id = de_id($request->comment_id);
        if(!$this->comment_model->where([
            'card_id' => $card_id,
            'id' => $comment_id
        ])->count()){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.comment_does_not_exist')
            ]);
        }
        $card = $this->card_model->where(['id' => $card_id])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }

        $content = $request->content;
        if(empty($content)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'nội dung'])
            ]);
        }

        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) &&
            !$this->note_model->is_note_owner($card->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $this->comment_model->card_id = $card_id;
        $this->comment_model->content = $content;
        $this->comment_model->user_id = $user['uid'];
        $this->comment_model->parent_id = $comment_id;
        if($this->comment_model->save()){
            $card_to_pop = $this->card_model->get_card( $card_id, $user['uid'] );
            $mentions = [];
            foreach (explode(' ', $content) as &$word) {
                if ($word[0] == '@') {
                    $mentions[] = substr($word, 1);
                }
            }
            $mentions_uid = $this->user_model->select('Id')->whereIn('Email', $mentions)->get();
            send_message_activeMQ(
                config('topic.task_comment.insert'),
                [
                    'card_id'   => $card_id,
                    'content'   => $content,
                    'user'      => $user['uid'],
                    'created_by'=> $card_to_pop->created_by,
                    'link'      => $card_to_pop->link,
                    'members'   => $card_to_pop->members,
                    'mentions'  => $mentions_uid
                ]
            );
            $file = $request->file('attachement');
            if ($file != null) {
                $file_extension = $file->getClientOriginalExtension();
                $path = $file->store('attachements');
                $file_name = $file->getClientOriginalName();
                $file_name = explode('.', $file_name);
                array_pop($file_name);
                $file_name = implode('.', $file_name );
                $file_name = remove_vn_accents($file_name);
                $this->file_model->file_name = $file_name;
                $this->file_model->file_type = $file_extension;
                $this->file_model->file_url = $path;
                $this->file_model->file_size = $file->getClientSize();
                $this->file_model->file_content_type = $file->getMimeType();
                $this->file_model->author = $user['uid'];
                if ($this->file_model->save()) {
                    $this->comment_files_model->comment_id = $this->comment_model->id;
                    $this->comment_files_model->file_id = $this->file_model->id;
                    $this->comment_files_model->save();
                }
                $this->comment_model->file = [
                    'file_id' => en_id($this->file_model->id),
                    'file_name' => $file_name,
                    'file_type' => $file_extension,
                    'file_url' => "file/".en_id($this->file_model->id),
                    'file_size' => $file->getClientSize(),
                    'file_content_type' => $file->getMimeType(),
                ];
            }
            send_json([
                'status' => 1,
                'data' => new CommentsResource($this->comment_model)
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_comment')
            ]);
        }
    }

    public function destroy(Request $request){
        if(empty($request->comment_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'bình luận'])
            ]);
        }

        $comment_id = de_id($request->comment_id);
        if(!$this->comment_model->where(['id' => $comment_id])->count()){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.comment_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        if(!$this->comment_model->where([
            'id' => $comment_id,
            'user_id' => $user['uid']
        ])->count()){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        if($this->comment_model->where([
            'id' => $comment_id,
            'user_id' => $user['uid']
        ])->delete()){
            $this->comment_model->where(['parent_id' => $comment_id])->delete();
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_remove_comment')
            ]);
        }else{
            send_json([
                'status' => 1,
                'msg' => __('api_validation.fail_to_remove_comment')
            ]);
        }
    }
}
