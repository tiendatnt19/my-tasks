<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cards;
use App\notes;
use App\users;
use App\usergroups;
use App\user_in_group;
use App\favorite_group;
use App\group_view_history;
use App\Http\Resources\Users as UsersResource;
use DB;
class GroupsController extends Controller{
    public function __construct(){
        $this->note_model               = new notes;
        $this->card_model               = new cards;
        $this->user_model               = new users;
        $this->groups_model             = new usergroups;
        $this->user_group_model         = new user_in_group;
        $this->favorite_group_model     = new favorite_group;
        $this->group_view_history_model = new group_view_history;
    }

    public function add_plan(Request $request){
        $user = $request->get('user');
        $this->groups_model->Name = $request->input('Name');
        if (empty($this->groups_model->Name)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Name'])
            ]);
        }
        $this->groups_model->Status = $request->input('Status');
        if (!isset($this->groups_model->Status)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Status'])
            ]);
        }
        $this->groups_model->Status = (int) $request->input('Status');
        $this->groups_model->Description = $request->input('Description');
        $this->groups_model->from = "manual";
        $this->groups_model->source_id = 0;
        $this->groups_model->timestamps = false;
        if ($this->groups_model->save()) {
            $this->user_model->add_user_in_group([
                'UserId' => $user['uid'],
                'UserGroupId' => $this->groups_model->id,
                'level' => 2
            ]);
            send_message_activeMQ(
                config('topic.group.insert'),
                $this->groups_model
            );
            send_message_activeMQ(
                config('topic.user_group.insert'),
                [
                    'group_id' => $this->groups_model->id,
                    'uid' => $user['uid'],
                    'level' => 2
                ]
            );
            send_json([
                'status' => 1,
                'data' => [
                    'Id' => en_id($this->groups_model->id),
                    'Name' => $this->groups_model->Name,
                    'level' => en_id(2),
                    'Status' => $this->groups_model->Status,
                    'is_favorite' => 0,
                    'from' => 'manual',
                    'admin' => en_id($user['uid']),
                    'is_temple' => 0
                ]
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_create_plan')
            ]);
        }
    }

    public function add_member_to_plan(Request $request){
        $user = $request->get('user');
        $group_id = $request->group_id;
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $user_id = $request->user_id;
        if (empty($user_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $user_id = de_id($user_id);
        $group_to_add = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }
        $user_to_add = $this->user_model->where(['Id' => $user_id])->first();
        if ( empty($user_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.user_does_not_exist')
            ]);
        }

        // Kiểm tra user hiện tại có phải là admin của kế hoạch hay không
        if (!$this->user_group_model->is_admin_of_group($group_id, $user['uid']) || $group_to_add->from === 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        // Kiểm tra user cần thêm đã nằm trong nhóm hay chưa
        if($this->user_group_model->is_user_in_group($group_id, $user_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.user_already_in_group')
            ]);
        }

        // Thêm user và group
        if ($this->user_group_model->add_member($group_id, $user_id)) {
            send_message_activeMQ(
                config('topic.user_group.insert'),
                [
                    'group_id' => $group_id,
                    'uid' => $user_id,
                    'level' => 1
                ]
            );
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_member', [
                    'member' => $user_to_add->fullname,
                    'card' => $group_to_add->Name
                ])
            ]);
        }
    }

    public function update_plan(Request $request){
        $user = $request->get('user');
        $group_id = $request->Id;
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $group_to_add = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }

        // Kiểm tra user hiện tại có phải là admin của kế hoạch hay không
        if (!$this->user_group_model->is_admin_of_group($group_id, $user['uid']) || $group_to_add->from === 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        $update_data = [];
        if (!empty($request->Name)) {
            $update_data['Name'] = $request->Name;
        }
        if (isset($request->Status)) {
            $update_data['Status'] = (int) $request->Status > 0 ? 1 : 0 ;
        }
        if (!empty($request->Description)) {
            $update_data['Description'] = $request->Description;
        }
        // Cập nhật group
        $this->groups_model->timestamps = false;
        if ($this->groups_model->where(['Id' => $group_id])->update($update_data)) {
            $update_data['group_id'] = $group_id;
            send_message_activeMQ(
                config('topic.user_group.update'),
                $update_data
            );
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_update_plan'),
            ]);
        }
    }

    public function delete_plan(Request $request, $group_id){
        $user = $request->get('user');
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $group_to_add = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }

        // Kiểm tra user hiện tại có phải là admin của kế hoạch hay không
        if (!$this->user_group_model->is_admin_of_group($group_id, $user['uid']) || $group_to_add->from === 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        // Cập nhật group
        $this->groups_model->timestamps = false;
        if ($this->groups_model->where(['Id' => $group_id])->delete()) {
            send_message_activeMQ(
                config('topic.user_group.delete'),
                ['group_id' => $group_id]
            );
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_remove_plan'),
            ]);
        }
    }

    public function remove_plan_member(Request $request){
        $user = $request->get('user');
        $group_id = $request->group_id;
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $user_id = $request->user_id;
        if (empty($user_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $user_id = de_id($user_id);
        $group_to_add = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }
        $user_to_add = $this->user_model->where(['Id' => $user_id])->first();
        if ( empty($group_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.user_does_not_exist')
            ]);
        }

        // Kiểm tra user hiện tại có phải là admin của kế hoạch hay không
        if (!$this->user_group_model->is_admin_of_group($group_id, $user['uid']) || $group_to_add->from === 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
         if ($user['uid'] == $user_id) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_remove_your_self')
            ]);
        }

        // Kiểm tra user cần thêm đã nằm trong nhóm hay chưa
        if(!$this->user_group_model->is_user_in_group($group_id, $user_id)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.user_not_in_group')
            ]);
        }

        // Loại thành viên khỏi group
        if ($this->user_group_model->where([
            "UserGroupId" => $group_id, 
            "UserId" => $user_id
        ])->delete()) {
            send_message_activeMQ(
                config('topic.user_group.remove'),
                [
                    'group_id' => $group_id,
                    'uid' => $user_id
                ]
            );
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_remove_member', [
                    'member' => $user_to_add->fullname,
                    'card' => $group_to_add->Name
                ])
            ]);
        }
    }

    public function toggle_favorite_plan(Request $request){
        $user = $request->get('user');
        $group_id = $request->group_id;
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $group_to_add = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group_to_add) && $group_id != 0 && $group_id != -1 ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist'),
            ]);
        }
        // Kiểm tra user cần thêm đã nằm trong nhóm hay chưa
        if(!$this->user_group_model->is_user_in_group( $group_id, $user['uid'] ) && $group_id != 0 && $group_id != -1){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        $favorite_plan = $this->favorite_group_model->where(['group_id' => $group_id, 'user_id' => $user['uid']])->first();
        if (!empty($favorite_plan)) {
            if( $this->favorite_group_model->where(['group_id' => $group_id, 'user_id' => $user['uid']])->delete() ){
                send_json([
                    'status' => 1,
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.fail_to_add_favorite_plan')
                ]);
            }
        }else{
            if($this->favorite_group_model->insert([
                'group_id' => $group_id, 
                'user_id' => $user['uid']
            ])){
                send_json([
                    'status' => 1,
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.fail_to_add_favorite_plan')
                ]);
            }
        }
    }

    public function toogle_plan_status(Request $request){
        $user = $request->get('user');
        $group_id = $request->group_id;
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $group_to_add = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group_to_add) ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }

        // Kiểm tra user hiện tại có phải là admin của kế hoạch hay không
        if (!$this->user_group_model->is_admin_of_group($group_id, $user['uid']) || $group_to_add->from === 'core') {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        $this->groups_model->timestamps = false;
        if($this->groups_model->where(['Id' => $group_id])->update([
            'Status' => (int) !$group_to_add->Status
        ])){
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg'    => __('api_validation.fail_to_update_plan')
            ]);
        }
    }

    public function get_plans(Request $request){
        $user = $request->get('user');
        $groups = $this->user_model->get_groups_involved_in($user['uid']);
        $has_my_staff = false;
        foreach ($groups as &$group) {
            if ($group->level == 2) {
                $has_my_staff = true;
            }
            $group->level = en_id($group->level);
            $admin = $this->user_group_model->where([
                'UserGroupId' => $group->Id,
                'Level' => 2
            ])->first();
            // $group->admin = $admin;
            $group->admin = en_id($admin['UserId']);
            $group->is_favorite = $this->favorite_group_model->where(['group_id' => $group->Id, 'user_id' => $user['uid']])->count();
            $group->Id = en_id($group->Id);
            $group->is_temple = 0;
            unset($group->UserGroupId);
            unset($group->Type);
            unset($group->source_id);
        }
        if ($has_my_staff) {
            $groups[] = (object) [
                'Id' => en_id(-1),
                'Name' => __('template.my_staff'),
                'level' => en_id(2),
                'admin' => en_id($user['uid']),
                'is_temple' => 1,
                'Status' => 0,
                'is_favorite' => $this->favorite_group_model->where(['group_id' => -1, 'user_id' => $user['uid']])->count()
            ];
        }
        $groups[] = (object) [
            'Id' => en_id(0),
            'Name' => __('template.my_board'),
            'level' => en_id(2),
            'admin' => en_id($user['uid']),
            'is_temple' => 1,
            'Status' => 0,
            'is_favorite' => $this->favorite_group_model->where(['group_id' => 0, 'user_id' => $user['uid']])->count()
        ];
        send_json([
            'status' => count($groups),
            'data' => $groups
        ]); 
    }

    public function get_favorites_plans(Request $request){
        $user = $request->get('user');
        $groups = $this->groups_model->select("*")
            ->join("favorite_group", 'favorite_group.group_id', '=', 'usergroups.Id')
            ->where(DB::raw('favorite_group.user_id'), '=', $user['uid'])
            ->get();
        foreach ($groups as &$group) {
            $admin = $this->user_group_model->where([
                'UserGroupId' => $group->Id,
                'Level' => 2
            ])->first();
            $group->not_start_yet     = $this->note_model->get_cards_not_start($group->Id, $user['uid']);
            $group->on_working        = $this->note_model->get_cards_on_working($group->Id, $user['uid']);
            $group->done              = $this->note_model->get_cards_done($group->Id, $user['uid']);
            $group->missing_deadline  = $this->note_model->get_cards_missing_deadline($group->Id, $user['uid']);
            $group->admin = en_id($admin['UserId']);
            $group->is_favorite = 1;
            $group->Id = en_id($group->Id);
            $group->is_temple = 0;
            unset($group->id);
            unset($group->user_id);
            unset($group->group_id);
            unset($group->Type);
            unset($group->source_id);
        }
        if ($this->favorite_group_model->where(['group_id' => 0, 'user_id' => $user['uid']])->count()) {
            $groups[] = (object) [
                'Id' => en_id(0),
                'Name' => __('template.my_board'),
                'admin' => en_id($user['uid']),
                'is_temple' => 1,
                'Status' => 0,
                'is_favorite' => 1,
                'not_start_yet'     => $this->note_model->get_cards_not_start(0, $user['uid']),
                'on_working'        => $this->note_model->get_cards_on_working(0, $user['uid']),
                'done'              => $this->note_model->get_cards_done(0, $user['uid']),
                'missing_deadline'  => $this->note_model->get_cards_missing_deadline(0, $user['uid']),
            ];
        }
        if ($this->favorite_group_model->where(['group_id' => -1, 'user_id' => $user['uid']])->count()) {
            $my_staffs = [
                'Id'                => en_id(-1),
                'Name'              => __('template.my_staff'),
                'admin'             => en_id($user['uid']),
                'is_temple'         => 1,
                'Status'            => 0,
                'is_favorite'       => 1,
                'not_start_yet'     => 0,
                'on_working'        => 0,
                'done'              => 0,
                'missing_deadline'  => 0,
            ];
            $my_staff = $this->user_model->get_my_staff($user['uid']);
            foreach ($my_staff as $u) {
                $user_cards = $this->card_model->get_user_cards($u->Id);
                foreach ($user_cards as &$card) {
                    if($card->status == 0) $my_staffs['not_start_yet'] ++;
                    if($card->status == 1) {
                        $my_staffs['on_working'] ++;
                        if (strtotime($card->expiry_date) - strtotime('now') ) {
                            $my_staffs['missing_deadline'] ++;
                        }
                    }
                    if($card->status == 2) $my_staffs['done'] ++;
                }
            }
            $groups[] = (object) $my_staffs;
        }
        send_json([
            'status' => count($groups) > 0 ? 1 : 0,
            'data' => $groups
        ]); 
    }

    public function get_plan_statistic(Request $request, $group_id){
        $user = $request->get('user');
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg'    => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = de_id($group_id);
        $group = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group) && $group_id > 0) {
            send_json([
                'status' => 0,
                'msg'    => __('api_validation.plan_does_not_exist')
            ]);
        }else if (empty($group)) {
            $group = (object) [
                'Name'          => __('template.my_board'),
                'Status'        => 0,
                'Description'   => '',
                'Type'          => 0
            ];
        }
        $data = [
            'group' => [
                'Name'          => $group->Name,
                'Status'        => $group->Status,
                'Description'   => $group->Description,
                'Type'          => $group->Type,
            ],
            'notes' => $this->note_model->get_notes_and_count_its_cards($group_id, $user['uid'])
        ];

        $data['statistic'] = [
            'not_start_yet'     => $this->note_model->get_cards_not_start($group_id, $user['uid']),
            'on_working'        => $this->note_model->get_cards_on_working($group_id, $user['uid']),
            'done'              => $this->note_model->get_cards_done($group_id, $user['uid']),
            'missing_deadline'  => $this->note_model->get_cards_missing_deadline($group_id, $user['uid']),
        ];
        if ($group_id) {
            $data['members'] = $this->note_model->get_member_cards($group_id, $user['uid']);
        }

        send_json([
            'status' => 1,
            'data' => $data
        ]); 
    }

    public function get_core_plan_statistic(Request $request, $group_id){
        $user = $request->get('user');
        if (empty($group_id)) {
            send_json([
                'status' => 0,
                'msg'    => __('api_validation.missing_argv', ['argv' => 'Kế hoạch'])
            ]);
        }
        $group_id = (int)$group_id;
        $group = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group) && $group_id > 0) {
            send_json([
                'status' => 0,
                'msg'    => __('api_validation.plan_does_not_exist')
            ]);
        }else if (empty($group)) {
            $group = (object) [
                'Name'          => __('template.my_board'),
                'Status'        => 0,
                'Description'   => '',
                'Type'          => 0
            ];
        }
        $data = [
            'group' => [
                'Name'          => $group->Name,
                'Status'        => $group->Status,
                'Description'   => $group->Description,
                'Type'          => $group->Type,
            ],
            'notes' => $this->note_model->get_notes_and_count_its_cards($group_id, $user['uid'])
        ];

        $data['statistic'] = [
            'not_start_yet'     => $this->note_model->get_cards_not_start($group_id, $user['uid']),
            'on_working'        => $this->note_model->get_cards_on_working($group_id, $user['uid']),
            'done'              => $this->note_model->get_cards_done($group_id, $user['uid']),
            'missing_deadline'  => $this->note_model->get_cards_missing_deadline($group_id, $user['uid']),
        ];
        if ($group_id) {
            $data['members'] = $this->note_model->get_member_cards($group_id, $user['uid']);
        }

        send_json([
            'status' => 1,
            'data' => $data
        ]); 
    }
}
