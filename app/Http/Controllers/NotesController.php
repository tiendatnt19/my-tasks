<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\notes;
use App\cards;
use App\checklists;
use App\comments;
use App\users;
use App\card_labels;
use App\repeat_task;
use App\files;
use App\usergroups;
use App\user_in_group;
use App\group_view_history;
use App\Http\Resources\Files as FilesResource;
use App\Http\Resources\Cards as CardsResource;
use App\Http\Resources\Notes as NotesResource;
use App\Http\Resources\Checklists as ChecklistsResource;
use App\Http\Resources\Users as UsersResource;
use App\Http\Resources\Card_labels as LabelResource;

class NotesController extends Controller{
    public function __construct(){
        $this->note_model = new notes;
        $this->card_model = new cards;
        $this->checklist_model = new checklists;
        $this->comment_model = new comments;
        $this->user_model = new users;
        $this->file_model = new files;
        $this->groups_model = new usergroups;
        $this->user_group_model = new user_in_group;
        $this->label_model = new card_labels;
        $this->group_history_model = new group_view_history;
        $this->schedule_model = new repeat_task;
    }

    public function index(Request $request, $group_id){
        $user = $request->get('user');
        $group_id = de_id($group_id);
        
        $group = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group) && $group_id > 0 ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }
        $this->group_history_model->timestamps = false;
        $history = $this->group_history_model->where(['group_id' => $group_id, 'user_id' => $user['uid']])->first();
        if (!empty($history)) {
            $this->group_history_model
                ->where([
                    'group_id' => $group_id, 
                    'user_id' => $user['uid']
                ])
                ->update(['view_date' => date("Y-m-d H:i:s")]);
        }else{
            $this->group_history_model->insert([
                'group_id' => $group_id, 
                'user_id' => $user['uid'],
                'view_date' => date("Y-m-d H:i:s")
            ]);
        }
        $notes = $this->note_model->get_all_notes_user_can_access($user['uid'], $group_id);

        $card_ids = [];
        foreach ($notes as &$note) {
            $cards = $this->card_model
            ->where([
                'note_id' => $note->id
            ])
            // ->limit(10)
            ->orderBy('position', 'desc')
            ->orderBy('updated_at', 'desc')
            ->get();
            foreach ($cards as &$card) {
                $card_ids[] = $card->id;
                $card->todolist = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $card->id])->orderBy('id', 'asc')->paginate(50));
                $card->totalComment = $this->comment_model->where(['card_id' => $card->id])->count();
                $card->members = UsersResource::collection($this->user_model->get_card_member($card->id));
                $card->files = FilesResource::collection($this->file_model->get_files_for_card($card->id));
                $card->labels = LabelResource::collection($this->label_model->where(['card_id' => $card->id])->get());
                $schedule = $this->schedule_model->select('value')->where(['card_id' => $card->id, 'user_id' => $user['uid']])->first();
                $card->schedule = empty($schedule) ? '' : $schedule->value;
            }
            $note->cards = CardsResource::collection($cards);
            $note->note_id = en_id($note->id);
            $note->owner_id = en_id($note->owner_id);
            $note->group_id = en_id($note->group_id);
            unset($note->id);
        }
        if ($group_id == 0) {
            $my_synce_data = [
                'note_id' => en_id(0),
                'owner_id' => en_id($user['uid']),
                'group_id' => en_id($group_id),
                'title' => __('template.synce_from_other_plan'),
                'slug' => remove_vn_accents(__('template.synce_from_other_plan')),
                'type' => 'synce_from_plans',
                'position' => 1000
            ];
            $my_synce_cards = $this->card_model->get_plan_synce_cards($card_ids, $user['uid']);
            foreach ($my_synce_cards as &$card) {
                $card->todolist = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $card->id])->orderBy('id', 'asc')->paginate(50));
                $card->totalComment = $this->comment_model->where(['card_id' => $card->id, 'parent_id' => 0])->count();
                $card->members = UsersResource::collection($this->user_model->get_card_member($card->id));
                $card->files = FilesResource::collection($this->file_model->get_files_for_card($card->id));
                $card->labels = LabelResource::collection($this->label_model->where(['card_id' => $card->id])->get());
            }
            $my_synce_data['cards'] = CardsResource::collection($my_synce_cards);
            $notes[] = (Object) $my_synce_data;
            $sync_note = $this->note_model->where(['id' => 1])->first();
            $sync_cards = $this->card_model->where(['created_by' => (int) $user['uid']])->orderBy('updated_at', 'desc')->get();
            foreach ($sync_cards as &$card) {
                $card->todolist = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $card->id])->orderBy('id', 'asc')->paginate(50));
                $card->totalComment = $this->comment_model->where(['card_id' => $card->id, 'parent_id' => 0])->count();
                $card->members = UsersResource::collection($this->user_model->get_card_member($card->id));
                $card->files = FilesResource::collection($this->file_model->get_files_for_card($card->id));
                $card->labels = LabelResource::collection($this->label_model->where(['card_id' => $card->id])->get());
            }
            $sync_note['cards'] = CardsResource::collection($sync_cards);
            $notes[] = (Object) $sync_note;
        }
        if ($group_id == -1) {
            $my_staff = $this->user_model->get_my_staff($user['uid']);
            $i = 1;
            foreach ($my_staff as $u) {
                $user_data = [
                    'note_id' => en_id(0),
                    'owner_id' => en_id($u->Id),
                    'group_id' => en_id(-1),
                    'title' => $u->Fullname,
                    'slug' => remove_vn_accents($u->Fullname),
                    'type' => 'my-staff',
                    'position' => $i++
                ];
                $user_cards = $this->card_model->get_user_cards($u->Id);
                foreach ($user_cards as &$card) {
                    $card->todolist = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $card->id])->orderBy('id', 'asc')->paginate(50));
                    $card->totalComment = $this->comment_model->where(['card_id' => $card->id, 'parent_id' => 0])->count();
                    $card->members = UsersResource::collection($this->user_model->get_card_member($card->id));
                    $card->files = FilesResource::collection($this->file_model->get_files_for_card($card->id));
                    $card->labels = LabelResource::collection($this->label_model->where(['card_id' => $card->id])->get());
                }
                $user_data['cards'] = CardsResource::collection($user_cards);
                $notes[] = (Object) $user_data;
            }
            $sync_note = $this->note_model->where(['id' => 1])->first();
            $sync_cards = $this->card_model->where(['created_by' => (int) $user['uid']])->orderBy('updated_at', 'desc')->get();
            foreach ($sync_cards as &$card) {
                $card->todolist = ChecklistsResource::collection($this->checklist_model->where(['card_id' => $card->id])->orderBy('id', 'asc')->paginate(50));
                $card->totalComment = $this->comment_model->where(['card_id' => $card->id, 'parent_id' => 0])->count();
                $card->members = UsersResource::collection($this->user_model->get_card_member($card->id));
                $card->files = FilesResource::collection($this->file_model->get_files_for_card($card->id));
                $card->labels = LabelResource::collection($this->label_model->where(['card_id' => $card->id])->get());
            }
            $sync_note['cards'] = CardsResource::collection($sync_cards);
            $notes[] = (Object) $sync_note;
        }
        send_json([
            'status' => count($notes) > 0,
            'data' => $notes,
        ]);
    }

    public function get_only_notes(Request $request, $group_id){
        $user = $request->get('user');
        $group_id = de_id($group_id);
        
        $group = $this->groups_model->where(['Id' => $group_id])->first();
        if ( empty($group) && $group_id ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.plan_does_not_exist')
            ]);
        }
        $notes = $this->note_model->get_all_notes_user_can_access($user['uid'], $group_id);

        send_json([
            'status' => 1,
            'data' => NotesResource::collection($notes)
        ]);
    }

    public function store(Request $request){
        $this->note_model->title = $request->input('title');
        if (empty($this->note_model->title)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'tiêu đề'])
            ]);
        }
        $this->note_model->group_id = $request->input('group_id');
        if (empty($this->note_model->group_id)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'nhóm'])
            ]);
        }
        $this->note_model->group_id = de_id($this->note_model->group_id);
        if ( !is_numeric($this->note_model->group_id) || $this->note_model->group_id < 0 ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.group_id_not_valid')
            ]);
        }
        $user = $request->get('user');
        if ($this->note_model->group_id > 0) {
            $group = $this->groups_model->where(['Id' => $this->note_model->group_id])->first();
            if (!$group || empty($group)) {
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.group_does_not_exist')
                ]);
            }
            if (!$this->user_group_model->is_user_in_group($this->note_model->group_id, $user['uid'])) {
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.cannt_do_this_action')
                ]);
            }
        }
        $this->note_model->slug = remove_vn_accents($this->note_model->title);
        $i = 1;
        while( count( $this->note_model->where('slug', $this->note_model->slug)->get() ) ){
            $this->note_model->slug = remove_vn_accents($this->note_model->title) . '-' . $i++;
        }
        $this->note_model->owner_id = $user['uid'];
        if($this->note_model->save()){
            $this->note_model->cards = [];
            send_json([
                'status' => 1,
                'data' => new NotesResource($this->note_model)
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_add_note')
            ]);
        }
    }

    public function update(Request $request){
        // checking the request is put or post
        if( ! $request->isMethod('post') ){
            send_json([
                'status' => '0',
                'msg' => __('api_validation.method_not_valid')
            ]);
        }
        
        $user = $request->get('user');
        $note_to_update = $this->note_model->where([
            'id' => de_id($request->note_id)
        ])->first();
        if(empty($note_to_update)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        $is_admin_of_group = true;
        if((int)$note_to_update->group_id > 0 ) 
            $is_admin_of_group = $this->user_group_model->is_admin_of_group((int)$note_to_update->group_id, $user['uid']);
        if( 
            (int) $user['uid'] !== (int)$note_to_update->owner_id && 
            !$is_admin_of_group && 
            !empty($request->title)
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.not_allow_to_modify_this_note')
            ]);
        }
        $data = [];
        if(empty($request->title) && empty($request->position)){
            send_json([
                'status' => 0,
                'msg'   => __('api_validation.missing_argv', ['argv' => 'tiêu đề'])
            ]);
        }
        if (!empty($request->title)) {
            $data['title'] = $request->title;
            $data['slug'] = remove_vn_accents($request->title);
            while (count($this->note_model->where([
                [ 'slug', '=', $data['slug'] ]
            ])->get())) {
                $data['slug'] = remove_vn_accents($request->title) . '-' . time();
            }
        }
        if(!empty($request->position)){
            $data['position'] = (int) $request->position;
        } 
        if($this->note_model->whereId(de_id($request->note_id))->update($data)){
            send_json([
                'status' => 1,
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_update_note')
            ]);
        }
    }

    public function destroy(Request $request, $id){
        $user = $request->get('user');
        $note_to_delete = $this->note_model->where([
            'id' => de_id($id)
        ])->first();
        if(empty($note_to_delete)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.note_does_not_exist')
            ]);
        }
        if( 
            $user['uid'] != $note_to_delete->owner_id &&
            !$this->user_group_model->is_admin_of_group( $note_to_delete->group_id, $user['uid'] )
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $delete = $this->note_model->delete_note( de_id( $id ), $user['uid'] );
        if( $delete ){
            send_json([
                'status' => 1
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_delete_note')
            ]);
        }
    }
}
