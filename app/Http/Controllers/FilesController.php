<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\cards;
use App\notes;
use App\card_files;
use App\files;
use App\logs;
use App\file_permission;
use App\users;
use App\user_in_group;
use App\Http\Resources\Files as FilesResource;
use App\Http\Resources\Users as UsersResource;
use DB;
use Storage;
use Exception;
use Image;

class FilesController extends Controller{

    public function __construct(){
        $this->card_model = new cards;
        $this->note_model = new notes;
        $this->file_model = new files;
        $this->logs_model = new logs;
        $this->user_model = new users;
        $this->setting_model = new file_permission;
        $this->usergroup_model = new user_in_group;
        $this->card_files_model = new card_files;
    }
    public function index(Request $request, $card_id) {
        if(empty($card_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($card_id);
        $card_to_get = $this->card_model->where([
            'id' => $card_id
        ])->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($card_to_get)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) || 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) ||
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }

        send_json([
            'status' => 1,
            'data' => FilesResource::collection($this->file_model->get_files_for_card($card_id))
        ]);
    }

    public function store(Request $request){
        if ($request->file('attachement') == null) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'file đính kèm']),
            ]);
        }
        if(empty($request->card_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $card_to_add = $this->card_model->where([
            'id' => $card_id
        ])->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($card_to_add)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_add->note_id) &&
            !$this->note_model->is_note_owner($card_to_add->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $file = $request->file('attachement');
        $file_extension = $file->getClientOriginalExtension();
        $path = $file->store('attachements');
        $file_name = $file->getClientOriginalName();
        $file_name = explode('.', $file_name);
        array_pop($file_name);
        $file_name = implode('.', $file_name );
        $file_name = remove_vn_accents($file_name);
        
        $this->file_model->file_name = $file_name;
        $this->file_model->file_type = $file_extension;
        $this->file_model->file_url = $path;
        $this->file_model->file_size = $file->getClientSize();
        $this->file_model->file_content_type = $file->getMimeType();
        $this->file_model->author = $user['uid'];
        if($this->file_model->save()){
            $this->file_model->card_id = $card_id;
            $this->file_model->alias_name = $file_name;
            $this->card_files_model->card_id = $card_id;
            $this->card_files_model->file_id = $this->file_model->id;
            $this->card_files_model->alias_name = $file_name;
            $this->card_files_model->save();
            
            send_json([
                'status'    => 1,
                'data'      => new FilesResource($this->file_model),
                'acti_id'   => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $card_id,
                    'type' => config("constants.ADD_ATTACHEMENT"),
                    'new_value' => $file_name,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_upload_file')
            ]);
        }
    }
    
    public function delete_attchement(Request $request, $file_id) {
        if(empty($file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $file_id = de_id($file_id);
        $file_to_delete = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_delete)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $card_to_get = $this->card_model->where([
            'id' => $file_to_delete->card_id
        ])->first();
        $user = $request->get('user');
        if(
            $file_to_delete->author != $user['uid'] && 
            $card_to_get->created_by != $user['uid']
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        if(
            Storage::delete($file_to_delete->file_url) && 
            $this->file_model->whereId($file_id)->delete() && 
            $this->card_files_model->where(['card_id' => $file_to_delete->card_id, 'file_id' => $file_id])->delete()
        ){
            
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_remove_comment'),
                'acti_id' => en_id($this->logs_model->insertGetId([
                    'user_id' => $user['uid'],
                    'card_id' => $file_to_delete->card_id,
                    'type' => config("constants.REMOVE_ATTACHEMENT"),
                    'new_value' => $file_to_delete->file_name,
                ]))
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_delete_file')
            ]);
        }
    }

    public function move_attchement_to_card(Request $request) {
        if(empty($request->card_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $card_to_move_in = $this->card_model->where([
            'id' => $card_id
        ])->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($card_to_move_in)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if(empty($request->file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'file'])
            ]);
        }
        $file_id = de_id($request->file_id);
        $file_to_move = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_move)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        
        $card_to_get = $this->card_model->where([
            'id' => $file_to_move->card_id
        ])->first();
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($file_to_move->card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) &&
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        if($this->card_files_model->where(['card_id' => $card_id, 'file_id' => $file_id])->count()){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_already_exist', [
                    'card' => $card_to_move_in->card_title,
                    'file' => $file_to_move->file_name
                ])
            ]);
        }
        if($this->card_files_model->where([
            'card_id' => $file_to_move->card_id,
            'file_id' => $file_id
        ])->update([
            'card_id' => $card_id
        ])){
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_move_comment')
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_move_file')
            ]);
        }
    }

    public function copy_attchement_to_card(Request $request) {
        if(empty($request->card_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $card_id = de_id($request->card_id);
        $card_to_move_in = $this->card_model->where([
            'id' => $card_id
        ])->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($card_to_move_in)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if(empty($request->file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $file_id = de_id($request->file_id);
        $file_to_move = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_move)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $card_to_get = $this->card_model->where([
            'id' => $file_to_move->card_id
        ])->first();
        
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($file_to_move->card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) &&
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        if($this->card_files_model->where(['card_id' => $card_id, 'file_id' => $file_id])->count()){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_already_exist', [
                    'card' => $card_to_move_in->card_title,
                    'file' => $file_to_move->file_name
                ])
            ]);
        }
        
        $this->card_files_model->card_id = $card_id;
        $this->card_files_model->file_id = $file_id;
        if($this->card_files_model->save()){
            send_json([
                'status' => 1,
                'msg' => __('api_validation.success_to_move_comment')
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.fail_to_move_file')
            ]);
        }
    }

    public function download(Request $request, $file_id) {
        if(empty($file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $file_id = de_id($file_id);
        $file_to_download = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        if(empty($file_to_download)){ 
            $file_to_download = $this->file_model->select('files.*', 'comments.card_id')
                ->join('comment_files', 'comment_files.file_id', '=', 'files.id')
                ->join('comments', 'comment_files.comment_id', '=', 'comments.id')
                ->where(['files.id' => $file_id])
                ->first();
        }
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_download)){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $card_to_get = $this->card_model->where([
            'id' => $file_to_download->card_id
        ])->first();
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($file_to_download->card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) &&
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            die(__('api_validation.cannt_do_this_action'));
        }
        if (!$this->user_has_permission_with_file($file_id, $file_to_download->card_id, $user['uid'])) {
            die(__('api_validation.cannt_do_this_action'));
        }
        
        $application = [
            'aac' =>  'audio/aac',
            'abw' =>  'application/x-abiword',
            'arc' =>  'application/octet-stream',
            'avi' =>  'video/x-msvideo',
            'azw' =>  'pplication/vnd.amazon.ebook',
            'bin' =>  'application/octet-stream',
            'bmp' =>  'image/bmp',
            'bz' =>  'application/x-bzip',
            'bz2' =>  'application/x-bzip2',
            'csh' =>  'application/x-csh',
            'css' =>  'text/css',
            'csv' =>  'text/csv',
            'doc' =>  'application/msword',
            'docx' =>  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'eot' =>  'application/vnd.ms-fontobject',
            'epub' =>  'application/epub+zip',
            'es' =>  'application/ecmascript',
            'exe' => 'application/vnd.microsoft.portable-executable',
            'gif' =>  'image/gif',
            'htm' =>  'text/htm',
            'html' =>  'text/html',
            'ico' =>  'image/x-icon',
            'ics' =>  'text/calendar',
            'jar' =>  'application/java-archive',
            'jpeg' =>  'image/jpeg',
            'jpg' =>  'image/jpeg',
            'js' =>  'application/javascript',
            'json' =>  'application/json',
            'mid' =>  'audio/midi',
            'midi' =>  'audio/midi',
            'mpeg' =>  'video/mpeg',
            'mpkg' =>  'application/vnd.apple.installer+xml',
            'odp' =>  'application/vnd.oasis.opendocument.presentation',
            'ods' =>  'application/vnd.oasis.opendocument.spreadsheet',
            'odt' =>  'application/vnd.oasis.opendocument.text',
            'oga' =>  'audio/ogg',
            'ogv' =>  'video/ogg',
            'ogx' =>  'application/ogg',
            'otf' =>  'font/otf',
            'png' =>  'image/png',
            'pdf' =>  'application/pdf',
            'ppt' =>  'application/vnd.ms-powerpoint',
            'pptx' =>  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'rar' =>  'application/x-rar-compressed',
            'rtf' =>  'application/rtf',
            'sh' =>  'application/x-sh',
            'sql' => 'application/sql',
            'svg' =>  'image/svg+xml',
            'swf' =>  'application/x-shockwave-flash',
            'tar' =>  'application/x-tar',
            'tif' =>  'image/tiff',
            'tiff' =>  'image/tiff',
            'ts' =>  'application/typescript',
            'ttf' =>  'font/ttf',
            'txt' =>  'text/plain',
            'vsd' =>  'application/vnd.visio',
            'wav' =>  'audio/wav',
            'weba' =>  'audio/webm',
            'webm' =>  'video/webm',
            'webp' =>  'image/webp',
            'woff' =>  'font/woff',
            'woff2' =>  'font/woff2',
            'xhtml' =>  'application/xhtml+xml',
            'xls' =>  'application/vnd.ms-excel',
            'xlsx' =>  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xml' =>  'application/xml',
            'xul' =>  'application/vnd.mozilla.xul+xml',
            'zip' =>  'application/zip',
            '3gp' =>  'video/3gpp',
            '3g2' =>  'video/3gpp2',
            '7z' =>  'application/x-7z-compressed'
        ];
        $content_type = $file_to_download->file_content_type;
        if (empty($content_type)) {
            $content_type = $application[$file_to_download->file_type];
        }
        header("Content-type: ".$content_type);
        header("Content-Disposition: attachment; filename=".$file_to_download->file_name.".".$file_to_download->file_type); 
        $pdfiledata = file_get_contents(url('storage/app/'.$file_to_download->file_url));
        echo $pdfiledata;
    }

    private function user_has_permission_with_file($file_id, $card_id, $uid){
        $file = $this->file_model->where(['id' => $file_id])->first();
        if ($file->author == $uid) {
            return true;
        }
        $setting = $this->setting_model->where([
            'card_id' => $card_id,
            'file_id' => $file_id
        ])->first();
        if (!empty($setting)) {
            return in_array($uid, explode(',', $setting->user_ids));
        }else{
            return true;
        }
    }

    public function check_file_permission(Request $request, $file_id, $card_id){
        $file_id = de_id($file_id);
        $card_id = de_id($card_id);
        $user = $request->get('user');
        send_json([
            'status' => $this->user_has_permission_with_file($file_id, $card_id, $user['uid'])
        ]);
    }

    public function view(Request $request, $file_id) {
        if(empty($file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $file_id = de_id($file_id);
        $file_to_view = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_view)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $card_to_get = $this->card_model->where([
            'id' => $file_to_view->card_id
        ])->first();
        
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($file_to_view->card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) &&
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $img = url('storage/app/'.$file_to_view->file_url);
        if (!$this->user_has_permission_with_file($file_id, $file_to_view->card_id, $user['uid'])) {
            $img = url('img/no-photo.png');
            $file_to_view->file_type = 'png';
            $file_to_view->file_content_type = 'image/png';
        }
        $remote_file = $img;
        // Set a maximum height and width
        $width = 150;
        $height = 150;

        list($width_orig, $height_orig) = getimagesize($img);

        $ratio_orig = $width_orig/$height_orig;
        $height = $width/$ratio_orig;
        $image_p = imagecreatetruecolor($width, $height);
        switch ( strtolower($file_to_view->file_type) ){
            case 'jpeg':
            case 'jpg':
                $image = imagecreatefromjpeg($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagejpeg($image_p, NULL, 100);
                imagedestroy($image_p);
                break;
            case 'png':
                $image = imagecreatefrompng($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width*1.2, $height*1.2, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagepng($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
            case 'gif':
                $image = imagecreatefromgif($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagegif($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
            case 'bmp':
                $image = imagecreatefrombmp($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagebmp($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
        }
    }

    public function thumbnail(Request $request, $file_id) {
        if(empty($file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $file_id = de_id($file_id);
        $file_to_view = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_view)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $card_to_get = $this->card_model->where([
            'id' => $file_to_view->card_id
        ])->first();
        
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($file_to_view->card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) &&
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $img = url('storage/app/'.$file_to_view->file_url);
        if (!$this->user_has_permission_with_file($file_id, $file_to_view->card_id, $user['uid'])) {
            $img = url('img/no-photo.png');
            $file_to_view->file_type = 'png';
            $file_to_view->file_content_type = 'image/png';
        }
        $remote_file = $img;
        // Set a maximum height and width
        $width = 280;
        $height = 280;

        list($width_orig, $height_orig) = getimagesize($img);

        $ratio_orig = $width_orig/$height_orig;
        $height = $width/$ratio_orig;
        $image_p = imagecreatetruecolor($width, $height);
        switch ( strtolower($file_to_view->file_type) ){
            case 'jpeg':
            case 'jpg':
                $image = imagecreatefromjpeg($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagejpeg($image_p, NULL, 100);
                imagedestroy($image_p);
                break;
            case 'png':
                $image = imagecreatefrompng($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width*1.2, $height*1.2, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagepng($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
            case 'gif':
                $image = imagecreatefromgif($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagegif($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
            case 'bmp':
                $image = imagecreatefrombmp($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagebmp($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
        }
    }

    public function view_full(Request $request, $file_id) {
        if(empty($file_id)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.missing_argv', ['argv' => 'thẻ'])
            ]);
        }
        $file_id = de_id($file_id);
        $file_to_view = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id
            ])
            ->first();
        if(empty($file_to_view)){ 
            $file_to_view = $this->file_model->select('files.*', 'comments.card_id')
                ->join('comment_files', 'comment_files.file_id', '=', 'files.id')
                ->join('comments', 'comment_files.comment_id', '=', 'comments.id')
                ->where(['files.id' => $file_id])
                ->first();
        }
        // kiểm tra thẻ có tồn tại hay không
        if(empty($file_to_view)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $card_to_get = $this->card_model->where([
            'id' => $file_to_view->card_id
        ])->first();
        
        $user = $request->get('user');
        if(
            !$this->card_model->is_a_card_member($file_to_view->card_id, $user['uid']) && 
            !$this->card_model->is_a_note_member($user['uid'], $card_to_get->note_id) &&
            !$this->note_model->is_note_owner($card_to_get->note_id, $user['uid'])
        ){
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $img = url('storage/app/'.$file_to_view->file_url);
        if (!$this->user_has_permission_with_file($file_id, $file_to_view->card_id, $user['uid'])) {
            $img = url('img/no-photo.png');
            $file_to_view->file_type = 'png';
            $file_to_view->file_content_type = 'image/png';
        }
        $remote_file = $img;
        list($width, $height) = getimagesize($img);

        $image_p = imagecreatetruecolor($width, $height);
        switch ( strtolower($file_to_view->file_type) ){
            case 'jpeg':
            case 'jpg':
                $image = imagecreatefromjpeg($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagejpeg($image_p, NULL, 100);
                imagedestroy($image_p);
                break;
            case 'png':
                $image = imagecreatefrompng($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagepng($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
            case 'gif':
                $image = imagecreatefromgif($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagegif($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
            case 'bmp':
                $image = imagecreatefrombmp($remote_file); 
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
                header('Content-Type: '.$file_to_view->file_content_type); 
                header("Content-Disposition: inline;filename=".$file_to_view->file_name.".".$file_to_view->file_type);
                imagebmp($image_p, NULL, 9);
                imagedestroy($image_p);
                break;
        }
    }

    public function get_file_setting(Request $request, $file_id, $card_id){
        $file_id = de_id($file_id);
        $card_id = de_id($card_id);
        $file_to_get = $this->file_model->select(DB::raw('files.*, card_files.alias_name'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id,
                'card_files.card_id' => $card_id
            ])
            ->first();
        // kiểm tra file có tồn tại hay không
        if(empty($file_to_get)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $setting = $this->setting_model->where([
            'card_id' => $card_id,
            'file_id' => $file_id
        ])->first();
        $file_setting = [
            'file_name' => $file_to_get->alias_name != null ? $file_to_get->alias_name  : $file_to_get->file_name ,
            'file_id' => en_id($file_id),
            'members' => []
        ];
        if (!empty($setting)) {
            $file_setting['members'] = UsersResource::collection($this->user_model->whereIn('ID', explode(',', $setting->user_ids))->get());
        }
        send_json([
            'status' => 1,
            'data' => $file_setting
        ]);
    }

    public function update_user_can_see_file(Request $request){
        $file_id = $request->file_id;
        $card_id = $request->card_id;
        // if(empty($file_id)){ 
        //     send_json([
        //         'status' => 0,
        //         'msg' => __('api_validation.missing_argv', ['argv' => 'file'])
        //     ]);
        // }
        // if(empty($file_id)){ 
        //     send_json([
        //         'status' => 0,
        //         'msg' => __('api_validation.missing_argv', ['argv' => 'file'])
        //     ]);
        // }
        $file_id = de_id($file_id);
        $card_id = de_id($card_id);
        $file = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id,
                'card_files.card_id' => $card_id
            ])
            ->first();
        // kiểm tra file có tồn tại hay không
        if(empty($file)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        $card = $this->card_model->where([
            'id' => $card_id
        ])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($file->author != (int)$user['uid'] && $card->created_by != (int)$user['uid'] ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $user_ids = $request->member_ids;
        foreach ($user_ids as &$id) {
            $id = (int)de_id($id);
        }
        $user_ids = array_unique($user_ids);
        $setting = $this->setting_model->where([
            'card_id' => $card_id,
            'file_id' => $file_id
        ])->first();
        if (empty($setting)) {
            $inserted_id = $this->setting_model->insertGetId([
                'card_id' => $card_id,
                'file_id' => $file_id,
                'user_ids' => implode(',', $user_ids)
            ]);
            if ($inserted_id) {
                send_json([ 'status' => 1 ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.error_occured')
                ]);
            }
        }else{
            $setting->user_ids = implode(',', $user_ids);
            $update = $setting->save();
            if ($update) {
                send_json([ 'status' => 1 ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => __('api_validation.error_occured')
                ]);
            }
        }
    }
    public function update_file_name(Request $request){
        $file_id = $request->file_id;
        $card_id = $request->card_id;
        $file_id = de_id($file_id);
        $card_id = de_id($card_id);
        $file = $this->file_model->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where([
                'files.id' => $file_id,
                'card_files.card_id' => $card_id
            ])
            ->first();
        // kiểm tra file có tồn tại hay không
        if(empty($file)){ 
            send_json([
                'status' => 0,
                'msg' => __('api_validation.file_does_not_exist')
            ]);
        }
        $user = $request->get('user');
        $card = $this->card_model->where([
            'id' => $card_id
        ])->first();
        if (empty($card)) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.card_does_not_exist')
            ]);
        }
        if ($file->author != (int)$user['uid'] && $card->created_by != (int)$user['uid'] ) {
            send_json([
                'status' => 0,
                'msg' => __('api_validation.cannt_do_this_action')
            ]);
        }
        $alias_name = $request->file_name;
        $card_file = $this->card_files_model->where([
            'card_id' => $card_id, 
            'file_id' => $file_id
        ])->first();
        $card_file->alias_name = $alias_name;
        $update = $card_file->save();
        if ($update) {
            send_json([ 'status' => 1 ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => __('api_validation.error_occured')
            ]);
        }
    }
}
