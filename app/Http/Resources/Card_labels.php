<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Card_labels extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){
        return [
            'label_id'       => en_id($this->id),
            'card_id'       => en_id($this->card_id),
            'label_name'    => $this->label_name,
            'label_pos'     => $this->label_pos,
            'check'         => $this->check,
        ];
    }
}
