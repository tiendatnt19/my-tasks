<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comments extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'comment_id'    => en_id($this->id),
            'content'       => $this->content,
            'user_id'       => en_id($this->user_id),
            'card_id'       => en_id($this->card_id),
            'parent_id'     => en_id($this->parent_id),
            'created_at'    => $this->created_at,
            'replies'       => $this->when( isset($this->replies) , $this->replies),
            'file'          => $this->when( isset($this->file) , $this->file),
        ];
    }
}
