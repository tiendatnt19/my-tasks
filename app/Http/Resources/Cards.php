<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Cards extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){
        return [
            'card_id'       => en_id($this->id),
            'note_id'       => en_id($this->note_id),
            'card_title'    => $this->card_title,
            'card_slug'     => $this->card_slug,
            'card_des'      => $this->card_des,
            'created_by'    => en_id($this->created_by),
            'status'        => $this->status,
            'show_des'      => $this->show_des,
            'is_urgent'     => $this->is_urgent,
            'show_checklist'=> $this->show_checklist,
            'position'      => $this->position,
            'start_date'    => $this->start_date,
            'expiry_date'   => $this->expiry_date,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'action'        => $this->action,
            'from_source'   => $this->from_source,
            'source_id'     => $this->source_id,
            'finish_by'     => en_id($this->finish_by),
            'todolist'      => $this->when( isset($this->todolist) , $this->todolist),
            'schedule'      => $this->when( isset($this->schedule) , $this->schedule),
            'totalComment'  => $this->when( isset($this->totalComment) , $this->totalComment),
            'members'       => $this->when( isset($this->members) , $this->members),
            'files'         => $this->when( isset($this->files) , $this->files),
            'labels'        => $this->when( isset($this->labels) , $this->labels),
            'group_id'      => $this->when( isset($this->group_id) , en_id($this->group_id))
        ];
    }
}
