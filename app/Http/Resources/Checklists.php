<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Checklists extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'todo_list_id'  => en_id($this->id),
            'card_id'       => en_id($this->card_id),
            'content'       => $this->content,
            'status'        => $this->status,
            'created_at'    => $this->created_at
        ];
    }
}
