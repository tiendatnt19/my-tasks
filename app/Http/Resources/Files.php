<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Files extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'file_id' => en_id($this->id),
            'alias_name' => $this->alias_name,
            'file_name' => $this->alias_name == null ? $this->file_name : $this->alias_name,
            'file_type' => $this->file_type,
            'file_content_type' => $this->file_content_type,
            'file_url' => "file/".en_id($this->id),
            'file_size' => $this->file_size,
            'author_id' => en_id($this->author),
            'card_id' => en_id($this->card_id)
        ];
    }
}
