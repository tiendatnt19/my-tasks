<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Logs extends JsonResource{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){
        return [
            'id'         => en_id($this->id),
            'user_id'    => en_id($this->user_id),
            'card_id'    => en_id($this->card_id),
            'type'       => $this->type,
            'content'    => $this->new_value,
            'created_at' => $this->created_at,
            'fullname'   => $this->when( isset($this->Fullname) , $this->Fullname)
        ];
    }
}
