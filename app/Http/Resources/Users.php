<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Users extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){
        return [
            'uid'           => en_id($this->Id),
            'fullname'      => $this->Fullname,
            'email'         => $this->Email,
            'phone'         => $this->Phone,
            'avatar'        => $this->Avatar,
            'status'        => $this->Status,
            'note'          => $this->Note
        ];
    }
}
