<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Notes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'note_id' => en_id($this->id),
            'title' => $this->title,
            'slug' => $this->slug,
            'owner_id' => en_id($this->owner_id),
            'type' => $this->type,
            'group_id' => en_id($this->position),
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
