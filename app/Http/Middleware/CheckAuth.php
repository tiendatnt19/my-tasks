<?php

namespace App\Http\Middleware;

use Closure;
use App\users;
use Illuminate\Support\Facades\Redirect;
class CheckAuth
{
    public $attributes;
    
    public function boot(Router $router){
        parent::boot($router);

        $router->model('users', '\App\Models\users');
    }

    public function handle($request, Closure $next){
        if (!$this->check_auth()) {
            $url = config('constants.DOMAIN')."/login/?url=".config('app.url');
            return Redirect::to($url);
        }
        $uid = $_COOKIE['user_id'];
        // $uid = 9;
        $user = users::where(['Id' => $uid])->first();
        $request->attributes->add(['user' => [
            'uid' => $uid,
            'data' => $user
        ]]);
        return $next($request);
    }

    private function check_auth(){
        if(!isset($_COOKIE['user_id'])){
            return false;
        }
        $uid = $_COOKIE['user_id'];
        $checksum = $_COOKIE['checksum'];
        if (empty($uid) || empty($checksum)) {
            return false;
        }
        return hash_hmac('sha512', 'user'.$uid,'doubledat_tan') == $checksum;
    }
}
