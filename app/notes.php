<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\cards;
use App\checklists;
use App\comments;
use App\users;
use App\card_labels;
use App\card_members;
use App\card_files;
use App\user_in_group;
use DB;
class notes extends Model {
    protected $table = 'notes';
    public function get_all_notes_user_can_access($uid, $group_id){
    	$usergroup = new user_in_group;
        $is_group_admin = $usergroup->is_admin_of_group($group_id, $uid);
    	$is_group_member = $usergroup->is_user_in_group($group_id, $uid);
    	// return $is_group_admin;
    	if ($is_group_admin || $is_group_member) {
    		return $this->where(
                [
                    'group_id' => $group_id
                ]
            )
            ->orderBy('position', 'desc')
            ->get();
    	}else{
    		return $this->where(
                [
                    'owner_id' => $uid,
                    'group_id' => $group_id
                ]
            )
            ->orderBy('position', 'desc')
            ->get();
    	}
    }

    public function is_note_owner($note_id, $uid){
        return $this->where([
            'id' => $note_id,
            'owner_id' => $uid
        ])->count();
    }

    public function delete_note($note_id, $uid){
		$cards_id = cards::select('id')->where(['note_id' => $note_id])->get();
		$ids = [];
        $cards_model = new cards;
		foreach ($cards_id as &$id) {
			$ids[] = $id->id;
            $card_to_pop = $cards_model->get_card($id->id, $uid);
            send_message_activeMQ(
                config('topic.task.remove'),
                $card_to_pop
            );
		}
		$this->whereId($note_id)->delete();
		card_labels::whereIn('card_id', $ids)->delete();
		card_files::whereIn('card_id', $ids)->delete();
		checklists::whereIn('card_id', $ids)->delete();
		comments::whereIn('card_id', $ids)->delete();
		card_members::whereIn('card_id', $ids)->delete();
		cards::whereIn('id', $ids)->delete();
		return true;
    }

    public function get_notes_and_count_its_cards($group_id, $uid=0){
        $where = ['group_id' => $group_id];
        if ($group_id == 0) {
            $where['owner_id'] = $uid;
        }
        $notes = $this->select('id','title')->where($where)->get();
        foreach ($notes as &$note) {
            $note->total_cards = cards::select(DB::raw('count(*) as total'))->where(['note_id' => $note->id])->first()->total;
        }
        return $notes;
    }

    public function get_cards_not_start($group_id, $uid=0){
        if ( (int) $group_id < 1) {
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=0 AND note_id IN (SELECT id FROM notes WHERE group_id=0 AND owner_id= $uid)";
        }else{
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=0 AND note_id IN (SELECT id FROM notes WHERE group_id=$group_id)";
        }
        return DB::select($sql)[0]->total;
    }

    public function get_cards_on_working($group_id, $uid=0){
        if ( (int) $group_id < 1) {
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=1 AND note_id IN (SELECT id FROM notes WHERE group_id=0 AND owner_id= $uid)";
        }else{
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=1 AND note_id IN (SELECT id FROM notes WHERE group_id=$group_id)";
        }
        return DB::select($sql)[0]->total;
    }

    public function get_cards_done($group_id, $uid=0){
        if ( (int) $group_id < 1) {
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=2 AND note_id IN (SELECT id FROM notes WHERE group_id=0 AND owner_id= $uid)";
        }else{
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=2 AND note_id IN (SELECT id FROM notes WHERE group_id=$group_id)";
        }
        return DB::select($sql)[0]->total;
    }

    public function get_cards_missing_deadline($group_id, $uid=0){
        if ( (int) $group_id < 1) {
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=1 AND DATE(expiry_date) < DATE(NOW()) AND note_id IN (SELECT id FROM notes WHERE group_id=0 AND owner_id= $uid)";
        }else{
            $sql = "SELECT count(*) as total FROM `cards` WHERE status=1 AND DATE(expiry_date) < DATE(NOW()) AND note_id IN (SELECT id FROM notes WHERE group_id=$group_id)";
        }
        return DB::select($sql)[0]->total;
    }

    public function get_member_cards($group_id, $uid=0){
        $user_model = new users;
        $data = [];
        return $user_model->get_users_in_group($group_id);
    }
}
