<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class files extends Model{
    public function get_files_for_card($card_id){
        return $this->select(DB::raw('files.*, card_files.card_id'))
            ->join('card_files', 'card_files.file_id', '=', 'files.id')
            ->where(['card_files.card_id' => $card_id])
            ->get();
    }
}
